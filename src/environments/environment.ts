export const environment = {
  production: false,
  apiUrl: window['cfgApiBaseUrl'],
  tokenWhitelistedDomains: [ new RegExp('localhost:8080') ],
  tokenBlacklistedRoutes: [ new RegExp('\/oauth\/token') ]
};
