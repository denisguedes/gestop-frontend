import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CapitalizeDirective } from '../shared/capitalize.directive';

@NgModule({  
  imports: [
    CommonModule
  ],
  declarations: [
    CapitalizeDirective
  ],
  exports: [
    CapitalizeDirective   
  ],
})
export class SharedModule { }
