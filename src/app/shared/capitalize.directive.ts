import { Directive, EventEmitter, HostListener, Output } from '@angular/core';

@Directive({
  selector: '[capitalize]',
})
export class CapitalizeDirective {

  @Output() ngModelChange: EventEmitter<any> = new EventEmitter();
  value: any;

  @HostListener('input', ['$event']) onInputChange($event) {
    this.value = this.removerAcentuacao($event.target.value.toUpperCase());
    this.ngModelChange.emit(this.value);
  }

  removerAcentuacao(palavra){    
    
    palavra = palavra.replace(new RegExp('[ÁÀÂÃ]','gi'), 'A');
    palavra = palavra.replace(new RegExp('[ÉÈÊ]','gi'), 'E');
    palavra = palavra.replace(new RegExp('[ÍÌÎ]','gi'), 'I');
    palavra = palavra.replace(new RegExp('[ÓÒÔÕ]','gi'), 'O');
    palavra = palavra.replace(new RegExp('[ÚÙÛ]','gi'), 'U');
    palavra = palavra.replace(new RegExp('[Ç]','gi'), 'C');
    palavra = palavra.replace(new RegExp('[Ñ]','gi'), 'N');
    return palavra;	

  }

}
