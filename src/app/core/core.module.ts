import { RelatorioService } from './../relatorios/relatorio.service';
import { RelatorioComponent } from './../financeiro/pagamento/relatorio/relatorio.component';
import { CargoService } from './../administrador/cargo/cargo.service';
import { PedidoService } from './../financeiro/pedido/pedido.service';
import { DataLocaleService } from './data-locale-service';
import { FornecedorService } from './../administrador/fornecedor/fornecedor.service';
import { ClienteService } from './../administrador/cliente/cliente.service';
import { NgModule, LOCALE_ID } from '@angular/core';
import { CommonModule, registerLocaleData } from '@angular/common';
import { RouterModule } from '@angular/router';
import { Title } from '@angular/platform-browser';
import localePt from '@angular/common/locales/pt';

import { ToastrModule } from 'ngx-toastr';
import { ConfirmationService } from 'primeng/api';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { ChartModule } from 'primeng/chart';
import { CalendarModule } from 'primeng/calendar';

import { MenuComponent } from './menu/menu.component';
import { PaginaNaoEncontradaComponent } from './pagina-nao-encontrada/pagina-nao-encontrada.component';
import { PaginaPrincipalComponent } from './pagina-principal/pagina-principal.component';
import { AcessoNaoAutorizadoComponent } from './acesso-nao-autorizado/acesso-nao-autorizado.component';

import { ErrorHandlerService } from './error-handler.service';
import { SegurancaService } from '../seguranca/seguranca.service';
import { SistemaHttp } from '../seguranca/sistema-http';
import { PaisService } from '../cadastro/pais/pais.service';
import { PermissaoService } from '../seguranca/permissao/permissao.service';
import { UsuarioService } from '../seguranca/usuario/usuario.service';
import { PerfilService } from './../seguranca/perfil/perfil.service';
import { CidadeService } from './../cadastro/cidade/cidade.service';
import { EstadoService } from './../cadastro/estado/estado.service';
import { ProdutoService } from '../administrador/produto/produto.service';
import { CategoriaService } from '../administrador/categoria/categoria.service';
import { FormsModule } from '@angular/forms';
import { FormaPagamentoService } from '../financeiro/forma-pagamento/forma-pagamento.service';
import { PagamentoService } from '../financeiro/pagamento/pagamento.service';
import { FuncionarioService } from '../administrador/funcionario/funcionario.service';

registerLocaleData(localePt);

@NgModule({
  imports: [
    //Importação do Angular
    CommonModule,
    RouterModule,

    //Importação do PrimeNG
    ConfirmDialogModule,
    BreadcrumbModule,
    ChartModule,
    FormsModule,
    CalendarModule,

    //Importação de terceiros
    ToastrModule.forRoot({
      timeOut: 10000,
      positionClass: 'toast-bottom-right',
      preventDuplicates: true,
    })
  ],
  declarations: [
    MenuComponent,
    PaginaNaoEncontradaComponent,
    PaginaPrincipalComponent,
    AcessoNaoAutorizadoComponent
  ],
  exports: [
    MenuComponent,
    ToastrModule,
    ChartModule,
    ConfirmDialogModule,
    CalendarModule,
    FormsModule
  ],
  providers: [
    //Provider Angular
    Title,
    { provide: LOCALE_ID, useValue: 'pt' },

    //Provider PrimeNG
    ConfirmationService,

    //Provider Criados
    SistemaHttp,
    DataLocaleService,
    ErrorHandlerService,
    SegurancaService,
    PaisService,
    EstadoService,
    CidadeService,
    PermissaoService,
    UsuarioService,
    PerfilService,
    ClienteService,
    FornecedorService,
    ProdutoService,
    CategoriaService,
    PedidoService,
    PagamentoService,
    FormaPagamentoService,
    CargoService,
    FuncionarioService,
    RelatorioService
  ]
})
export class CoreModule { }
