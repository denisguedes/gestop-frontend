export class DataLocaleService {

  minDate: Date;
  maxDate: Date;
  invalidDates: any;
  es: any;
  tr: any;

constructor(){
  this.es = {
    firstDayOfWeek: 1,
    dayNames: [ "domingo","segunda","terça","quarta","quinta","sexta","sábado" ],
    dayNamesShort: [ "dom","seg","ter","qua","qui","sex","sáb" ],
    dayNamesMin: [ "D","S","T","Q","Q","S","S" ],
    monthNames: [ "Janeiro","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro" ],
    monthNamesShort: [ "Jan","Fev","Mar","Abr","Mai","Jun","Jul","Ago","Set","Out","Nov","Dez" ],
    today: 'Hoy',
    clear: 'Borrar'
  }

  this.tr = {
    firstDayOfWeek : 1
  }

  let today = new Date();
  let month = today.getMonth();
  let year = today.getFullYear();
  let prevMonth = (month === 0) ? 11 : month -1;
  let prevYear = (prevMonth === 11) ? year - 1 : year;
  let nextMonth = (month === 11) ? 0 : month + 1;
  let nextYear = (nextMonth === 0) ? year + 1 : year;
  this.minDate = new Date();
  this.minDate.setMonth(prevMonth);
  this.minDate.setFullYear(prevYear);
  this.maxDate = new Date();
  this.maxDate.setMonth(nextMonth);
  this.maxDate.setFullYear(nextYear);

  let invalidDate = new Date();
  invalidDate.setDate(today.getDate() - 1);
  this.invalidDates = [today,invalidDate];

  }
}
