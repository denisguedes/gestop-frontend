import { DataLocaleService } from './../data-locale-service';
import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-pagina-principal',
  templateUrl: './pagina-principal.component.html',
  styleUrls: ['./pagina-principal.component.css']
})
export class PaginaPrincipalComponent implements OnInit {

  private items: MenuItem[];
  data: any;
  dataDe: Date;
  dataAte: Date;

  constructor(
    private title: Title,
    private locale: DataLocaleService
  ) {
    this.data = {
      labels: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
      datasets: [
          {
              label: '2018',
              data: [65, 59, 80, 81, 56, 55, 40, 90, 12, 34, 46, 100],
              fill: false,
              borderColor: '#4bc0c0'
          },
          {
              label: '2019',
              data: [28, 48, 40, 19, 86, 27, 90, 23, 65, 87, 98, 30],
              fill: false,
              borderColor: '#565656'
          }
      ]
  }
  }

  ngOnInit() {
    this.items = [
      {label:'Gestop'},
      {label:'Painel'},
    ];
    this.title.setTitle ('Gestop - Home');
    this.locale;
}

}
