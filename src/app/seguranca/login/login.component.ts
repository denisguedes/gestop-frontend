import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { SegurancaService } from '../seguranca.service';
import { ErrorHandlerService } from 'src/app/core/error-handler.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  constructor(
    private seguracaService: SegurancaService,
    private router: Router,
    private errorHandler: ErrorHandlerService,
  ) { }

  login(usuario: string, senha: string) {
    this.seguracaService.login(usuario, senha)
      .then(() => {
        this.router.navigate(['/gestop/principal']);
      })
      .catch(erro => {
        this.errorHandler.handle(erro);
      });
  }

}
