import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FormComponent } from './form/form.component';
import { ListComponent } from './list/list.component';
import { SegurancaGuard } from '../seguranca.guard';

const routes: Routes = [
    {
      path: 'gestop/permissao/novo',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['PERMISSAO_CADASTRAR']}
    },
    {
      path: 'gestop/permissao/listar',
      component: ListComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['PERMISSAO_CONSULTAR']}
    },
    {
      path: 'gestop/permissao/:codigo',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['PERMISSAO_ALTERAR']}
    },
    {
      path: 'gestop/permissao/:codigo/:consulta',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['PERMISSAO_CONSULTAR']}
    },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})

export class permissaoRoutingModule { }
