import { Component, OnInit, ViewChild } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';

import { MenuItem, LazyLoadEvent, ConfirmationService } from 'primeng/components/common/api';
import { ToastrService } from 'ngx-toastr';

import { PermissaoService, PermissaoFiltro } from '../permissao.service';
import { Permissao } from '../permissao';
import { SegurancaService } from 'src/app/seguranca/seguranca.service';
import { ErrorHandlerService } from 'src/app/core/error-handler.service';


@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  private items: MenuItem[];
  private quantidadeItemPorPagina: any[];
  private quantidadeItemPorPaginaSelecionada: any;
  private selecionados: Permissao[];
  private filtro = new PermissaoFiltro;
  private totalRegistros = 0;
  private codigoEntidade: number;
  private consultar = true;
  private rotaNovo = "/gestop/permissao/novo";
  private rotaPadrao = '/gestop/permissao/';
  @ViewChild('tabela') tabelaEntidade;

  constructor(
    private entidadeService: PermissaoService,
    private title: Title,
    private toastr: ToastrService,
    private errorHandler: ErrorHandlerService,
    private confirmation: ConfirmationService,
    private router: Router,
    private segurancaService: SegurancaService
  ) {}

  ngOnInit() {

    this.pesquisar();

    this.items = [
      {label:'Administrador'},
      {label:'Cadastro'},
      {label:'Listagem de Permissão'}
    ];

    this.quantidadeItemPorPagina = [
      {value:10},
      {value:20},
      {value:50},
    ]

    this.title.setTitle ('Administrador - Permissão');
  }

  pesquisar(pagina = 0) {
    this.filtro.pagina = pagina;
    this.filtro.itensPorPagina = this.quantidadeItemPorPaginaSelecionada ? this.quantidadeItemPorPaginaSelecionada.value : 10;

    this.entidadeService.pesquisar(this.filtro)
      .then(resultado => {
        this.totalRegistros = resultado.total;
        this.selecionados = resultado.selecionados;
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  excluir (codigo:number) {
    if(!this.codigoEntidade){
      this.toastr.warning('Selecione um item da lista!');
    }else{
      this.confirmation.confirm({
        message: 'Tem certeza que deseja excluir',
        accept: (() => {
            this.entidadeService.excluir(codigo)
              .then(() => {
                if(this.tabelaEntidade.first === 0){
                  this.pesquisar();
                }else {
                  this.tabelaEntidade.first = 0;
                }
                this.toastr.success('Item excluído com sucesso!');
              })
              .catch (erro => this.errorHandler.handle(erro));
        })
      });
    }
  }

  limparFiltro () {
    this.filtro = new PermissaoFiltro();
    this.pesquisar();
  }

  aoMudarPagina (event: LazyLoadEvent) {
    const pagina = event.first / event.rows;
    this.pesquisar(pagina);
  }

  selecionarEntidade (entidade){
    this.codigoEntidade = entidade.codigo;
  }

}
