import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FormComponent } from './form/form.component';
import { ListComponent } from './list/list.component';
import { SegurancaGuard } from '../seguranca.guard';

const routes: Routes = [
    {
      path: 'gestop/perfil/novo',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['PERFIL_CADASTRAR']}
    },
    {
      path: 'gestop/perfil/listar',
      component: ListComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['PERFIL_CONSULTAR']}
    },
    {
      path: 'gestop/perfil/:codigo',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['PERFIL_ALTERAR']}
    },
    {
      path: 'gestop/perfil/:codigo/:consulta',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['PERFIL_CONSULTAR']}
    },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})

export class perfilRoutingModule { }
