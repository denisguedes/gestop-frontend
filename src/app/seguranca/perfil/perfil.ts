import { Permissao } from '../permissao/permissao';

export class Perfil {
    codigo: number;      
    descricao: string; 
    permissoes = new Array<PerfilPermissao>();   
}

export class PerfilPermissao {
    codigo: number;
    permissao = new Permissao();

    constructor(permissao?: Permissao){
        this.permissao = permissao;
    }
}