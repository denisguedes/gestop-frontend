import { Injectable } from '@angular/core';

import { SegurancaService } from './seguranca.service';
import { environment } from '../../environments/environment';
import { SistemaHttp } from './sistema-http';

@Injectable()
export class LogoutService {

  tokensRenokeUrl: string;

  constructor(
    private http: SistemaHttp,
    private segurancaService: SegurancaService
  ) {
    this.tokensRenokeUrl = `${environment.apiUrl}/gestop-api/tokens/revoke`;
  }

  logout() {
    return this.http.delete(this.tokensRenokeUrl, { withCredentials: true })
      .toPromise()
      .then(() => {
        this.segurancaService.limparAccessToken();
      });
  }

}
