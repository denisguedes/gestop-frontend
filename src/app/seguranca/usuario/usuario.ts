import { Permissao } from '../permissao/permissao';
import { Perfil } from '../perfil/perfil';

export class Usuario {
    codigo: number;
    login: string;
    email: string;
    senha: string;
    perfil = new Perfil();
    permissoes = new Array<UsuarioPermissao>();
}

export class UsuarioPermissao {
    codigo: number;
    permissao = new Permissao();

    constructor(permissao?: Permissao){
        this.permissao = permissao;
    }
}
