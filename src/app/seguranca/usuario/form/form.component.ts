import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { MenuItem } from 'primeng/components/common/api';
import { ToastrService } from 'ngx-toastr';
import { ErrorHandlerService } from 'src/app/core/error-handler.service';
import { Permissao } from '../../permissao/permissao';
import { PermissaoService } from '../../permissao/permissao.service';
import { Usuario, UsuarioPermissao } from '../usuario';
import { UsuarioService } from '../usuario.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  private items: MenuItem[];
  private entidade = new Usuario();
  private consulta: boolean;
  private codigoEntidade: number;
  private rotaListar = "/gestop/usuario/listar";
  private permissoes: Permissao[];
  private permissoesSelecionadas: Permissao[] = [];

  //Campos para validação
  private email = true;
  private senha = true;
  private perfil = true;
  private login = true;

  constructor(
    private entidadeService: UsuarioService,
    private permissaoService: PermissaoService,
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private title: Title,
    private errorHandler : ErrorHandlerService,
  ) { }

  ngOnInit() {

    this.items = [
      {label:'Administrador'},
      {label:'Segurança'},
      {label:'Usuário'}
    ];

    this.codigoEntidade = this.route.snapshot.params['codigo'];
    this.consulta = this.route.snapshot.params['consulta'];

    if(this.codigoEntidade){
      this.buscar(this.codigoEntidade);
    }else{
      this.carregarPermissoes();
    }
  }

  incluir (form: FormControl) {
    this.entidadeService.incluir(this.entidade)
      .then( ()=>{
        this.toastr.success('Cadastro realizado com sucesso');
        this.limparFormulario(form);
        this.router.navigate([this.rotaListar]);
      })
      .catch(erro => {
        this.errorHandler.handle(erro);
        this.exibiErroFormulario(form);
      });
  }

  alterar (form: FormControl) {
    this.entidadeService.alterar(this.entidade)
      .then( dados => {
        this.entidade = dados;
        this.toastr.success('Alteração realizada com sucesso');
        this.router.navigate([this.rotaListar]);
      })
      .catch(erro => {
        this.errorHandler.handle(erro)
        this.exibiErroFormulario(form);
      });
  }

  buscar (codigo: number) {
    this.entidadeService.buscar(codigo)
      .then( dados => {
        this.entidade = dados;
        this.preencherPermissaoUsuario();
        this.carregarPermissoes();
        if(this.consulta){
          this.title.setTitle (`Administrador - Consulta de Usuário:${this.entidade.email}`);
        }else{
          this.title.setTitle (`Administrador - Alteração de Usuário :${this.entidade.email}`);
        }
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  salvar (form: FormControl) {
    this.preencherPermissaoUsuarioParaSalvar();
    if (this.entidade.codigo){
      this.alterar(form);
    } else {
      this.incluir(form);
    }
  }

  limparFormulario (form: FormControl){
    this.entidade = new Usuario();
    form.reset();
  }

  exibiErroFormulario(form: FormControl){

    if(!form.value.email) {
      this.email = false;
    }else {
      this.email = true;
    }

    if(!form.value.senha) {
      this.senha = false;
    }else {
      this.senha = true;
    }

    if(!form.value.perfil) {
      this.perfil = false;
    }else {
      this.perfil = true;
    }

    if(!form.value.login) {
      this.login = false;
    }else {
      this.login = true;
    }
  }

  carregarPermissoes() {
    this.permissaoService.listar()
      .then(dados => {
        this.permissoes = dados;
        this.removerPermissaoSelecionadaDeListaDePermissoes();
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  preencherPermissaoUsuarioParaSalvar(){

    this.entidade.permissoes = new Array<UsuarioPermissao>();

    for(let i = 0; i < this.permissoesSelecionadas.length; i++) {
      let permissao: Permissao = this.permissoesSelecionadas[i];
      let entidadePermissao = new UsuarioPermissao(permissao);
      this.entidade.permissoes.push(entidadePermissao);
    }
  }

  preencherPermissaoUsuario() {
    for(let i = 0; i < this.entidade.permissoes.length; i++) {
      let permissao = this.entidade.permissoes[i].permissao;
      this.permissoesSelecionadas.push(permissao);
    }
  }

  removerPermissaoSelecionadaDeListaDePermissoes(){
    for(let i = 0; i < this.permissoesSelecionadas.length; i++) {
      let permisao = this.permissoesSelecionadas[i]
      let index;
      for(let i=0; i < this.permissoes.length; i++){
        if(this.permissoes[i].codigo == permisao.codigo){
          index = this.permissoes.indexOf(this.permissoes[i]);
        }
      }
      this.permissoes.splice(index, 1);
    }
  }

  selecionarPerfil (perfil){
    this.entidade.perfil = perfil;
  }

}
