import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { JwtHelperService } from '@auth0/angular-jwt';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SegurancaService {

  private baseUrl = `${environment.apiUrl}/oauth/token`;
  private baseUrlUsuario = `${environment.apiUrl}/gestop-api/usuario`;
  private jwtPayload: any;

  constructor(
    private http: HttpClient,
    private jwtHelper: JwtHelperService
  ) {
    this.carregarToken();
  }

  //Realizar o login
  login(usuario: string, senha: string): Promise<void> {
    const headers = new HttpHeaders()
        .append('Content-Type', 'application/x-www-form-urlencoded')
        .append('Authorization', 'Basic YW5ndWxhcjpAbmd1bEByMA=='); //angular:@ngul@r0 codificado

    const body = `username=${usuario}&password=${senha}&grant_type=password`;

    return this.http.post<any>(this.baseUrl, body,
      { headers, withCredentials: true })
    .toPromise()
    .then(response => {
      this.armazenarToken(response.access_token);
    })
    .catch(response => {
      if (response.status === 400) {
        if (response.error.error === 'invalid_grant') {
          return Promise.reject('Usuário ou senha inválida!');
        }
      }

      return Promise.reject(response);
    });
  }

  //Obter o novo access token após a expiração do token
  obterNovoAccessToken(): Promise<void> {
    const headers = new HttpHeaders()
        .append('Content-Type', 'application/x-www-form-urlencoded')
        .append('Authorization', 'Basic YW5ndWxhcjpAbmd1bEByMA==');

    const body = 'grant_type=refresh_token';

    return this.http.post<any>(this.baseUrl, body,
        { headers, withCredentials: true })
      .toPromise()
      .then(response => {
        this.armazenarToken(response.access_token);
        return Promise.resolve(null);
      })
      .catch(response => {
        return Promise.resolve(null);
      });
  }

  //Verificar se o token está valido
  isAccessTokenInvalido() {
    const token = localStorage.getItem('token');
    return !token || this.jwtHelper.isTokenExpired(token);
  }

  //Pequisa se no array de authorites tem a permissão de acesso
  temPermissao(permissao: string) {
    return this.jwtPayload && this.jwtPayload.authorities.includes(permissao);
  }

  //Verificar se o usuario tem permissao para acessar a pagina, esta sendo usada no canActivate
  temQualquerPermissao(roles) {
    for (const role of roles) {
      if (this.temPermissao(role)) {
        return true;
      }
    }

    return false;
  }

  //Salva o token no local storage e carrega o payload
  private armazenarToken(token: string) {
    this.jwtPayload = this.jwtHelper.decodeToken(token);
    localStorage.setItem('token', token);
  }

  //Verifica se tem o token no local storage, caso tenha chama o metodo armezar para carregar o payload
  private carregarToken() {
    const token = localStorage.getItem('token');

    if (token) {
      this.armazenarToken(token);
    }
  }

  //remova o token do local storage e limpa o payload
  limparAccessToken() {
    localStorage.removeItem('token');
    this.jwtPayload = null;
  }

  //usuario que está logado
  usuarioLogado(): Promise<any>{
    const token = localStorage.getItem('token');
    const usuario = this.jwtHelper.decodeToken(token);

    return this.http.get(`${this.baseUrlUsuario}/email/${usuario.user_name}`)
      .toPromise();
  }

}
