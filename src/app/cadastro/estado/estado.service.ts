import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

import { environment } from 'src/environments/environment';
import { SistemaHttp } from 'src/app/seguranca/sistema-http';
import { Estado } from './estado';
import { CrudService } from 'src/app/core/crud-service';

export class EstadoFiltro {
  codigo: number;
  nome: string;
  pagina = 0;
  itensPorPagina: number;
}

@Injectable({
  providedIn: 'root'
})
export class EstadoService extends CrudService<Estado>{

  private baseUrl = `${environment.apiUrl}/gestop-api/estado`;

  constructor(protected http: SistemaHttp) {
    super(http, `${environment.apiUrl}/gestop-api/estado`);
  }

  pesquisar(filtro: EstadoFiltro): Promise<any> {

    let params = new HttpParams({
      fromObject: {
        page: filtro.pagina.toString(),
        size: filtro.itensPorPagina.toString()
      }
    });

    if(filtro.codigo) {
      params = params.append('codigo', filtro.codigo.toString());
    }

    if(filtro.nome) {
      params = params.append('nome', filtro.nome);
    }

    return this.http.get<any>(this.baseUrl, { params })
      .toPromise()
      .then( response => {
        const resultado = {
          selecionados: response.content,
          total: response.totalElements
         }
        return resultado;
      });
  }

}
