import { Pais } from '../pais/pais';

export class Estado {
    codigo: number;
    nome: string;
    sigla: string;
    pais = new Pais();
}
