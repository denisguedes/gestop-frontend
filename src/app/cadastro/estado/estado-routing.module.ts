
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SegurancaGuard } from 'src/app/seguranca/seguranca.guard';
import { FormComponent } from './form/form.component';
import { ListComponent } from './list/list.component';

const routes: Routes = [
    {
      path: 'gestop/estado/novo',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['ESTADO_CADASTRAR']}
    },
    {
      path: 'gestop/estado/listar',
      component: ListComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['ESTADO_CONSULTAR']}
    },
    {
      path: 'gestop/estado/:codigo',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['ESTADO_ALTERAR']}
    },
    {
      path: 'gestop/estado/:codigo/:consulta',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['ESTADO_CONSULTAR']}
    },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class EstadoRoutingModule { }
