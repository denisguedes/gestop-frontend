import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SegurancaGuard } from 'src/app/seguranca/seguranca.guard';
import { FormComponent } from './form/form.component';
import { ListComponent } from './list/list.component';

const routes: Routes = [
    {
      path: 'gestop/cidade/novo',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['CIDADE_CADASTRAR']}
    },
    {
      path: 'gestop/cidade/listar',
      component: ListComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['CIDADE_CONSULTAR']}
    },
    {
      path: 'gestop/cidade/:codigo',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['CIDADE_ALTERAR']}
    },
    {
      path: 'gestop/cidade/:codigo/:consulta',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['CIDADE_CONSULTAR']}
    },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class CidadeRoutingModule { }
