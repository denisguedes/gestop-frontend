import { CidadeRoutingModule } from './cidade-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import {InputTextModule} from 'primeng/inputtext';
import {ButtonModule} from 'primeng/button';
import {TableModule} from 'primeng/table';
import {TooltipModule} from 'primeng/tooltip';
import {FieldsetModule} from 'primeng/fieldset';
import {BreadcrumbModule} from 'primeng/breadcrumb';
import {RadioButtonModule} from 'primeng/radiobutton';
import {DropdownModule} from 'primeng/dropdown';

import { ComponentesModule } from 'src/app/componentes/componentes.module';
import { FormComponent } from './form/form.component';
import { ListComponent } from './list/list.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,

    ComponentesModule,
    CidadeRoutingModule,

    InputTextModule,
    ButtonModule,
    TableModule,
    TooltipModule,
    FieldsetModule,
    BreadcrumbModule,
    RadioButtonModule,
    DropdownModule

  ],
  declarations: [
    FormComponent,
    ListComponent
  ]
})
export class CidadeModule { }
