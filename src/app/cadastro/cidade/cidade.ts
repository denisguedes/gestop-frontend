import { Estado } from '../estado/estado';

export class Cidade {
    codigo: number;
    nome: string;
    dataCadastro: Date;
    dataAlteracao: Date;
    estado = new Estado();
}
