import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { MenuItem } from 'primeng/components/common/api';
import { ToastrService } from 'ngx-toastr';
import { ErrorHandlerService } from 'src/app/core/error-handler.service';
import { Cidade } from '../cidade';
import { CidadeService } from '../cidade.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  private items: MenuItem[];
  private entidade = new Cidade();
  private consulta: boolean;
  private codigoEntidade: number;
  private rotaListar = "/gestop/cidade/listar";

  //Campos para validação
  private nome = true;
  private sigla = true;
  private estado = true;
  private regiao = true;

  constructor(
    private entidadeService: CidadeService,
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private title: Title,
    private errorHandler : ErrorHandlerService,
  ) { }

  ngOnInit() {

    this.items = [
      {label:'Gestop'},
      {label:'Cadastro'},
      {label:'Cidade'}
    ];

    this.codigoEntidade = this.route.snapshot.params['codigo'];
    this.consulta = this.route.snapshot.params['consulta'];

    if(this.codigoEntidade){
      this.buscar(this.codigoEntidade);
    }

  }

  incluir (form: FormControl) {
    this.entidadeService.incluir(this.entidade)
      .then( ()=>{
        this.toastr.success('Cadastro realizado com sucesso');
        this.limparFormulario(form);
        this.router.navigate([this.rotaListar]);
      })
      .catch(erro => {
        this.errorHandler.handle(erro);
        this.exibiErroFormulario(form);
      });
  }

  alterar (form: FormControl) {
    this.entidadeService.alterar(this.entidade)
      .then( dados => {
        this.entidade = dados;
        this.toastr.success('Alteração realizada com sucesso');
        this.router.navigate([this.rotaListar]);
      })
      .catch(erro => {
        this.errorHandler.handle(erro)
        this.exibiErroFormulario(form);
      });
  }

  buscar (codigo: number) {
    this.entidadeService.buscar(codigo)
      .then( dados => {
        this.entidade = dados;
        if(this.consulta){
          this.title.setTitle (`Gestop - Consulta de Cidade:${this.entidade.nome}`);
        }else{
          this.title.setTitle (`Gestop - Alteração de Cidade :${this.entidade.nome}`);
        }
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  salvar (form: FormControl) {
    if (this.entidade.codigo){
      this.alterar(form);
    } else {
      this.incluir(form);
    }
  }

  limparFormulario (form: FormControl){
    this.entidade = new Cidade();
    form.reset();
  }

  exibiErroFormulario(form: FormControl){

    if(!form.value.nome) {
      this.nome = false;
    }else {
      this.nome = true;
    }

    if(!form.value.sigla) {
      this.sigla = false;
    }else {
      this.sigla = true;
    }

    if(!form.value.estado) {
      this.estado = false;
    }else {
      this.estado = true;
    }

    if(!form.value.regiao) {
      this.regiao = false;
    }else {
      this.regiao = true;
    }

  }

  selecionarEstado (estado){
    this.entidade.estado = estado;
  }

}
