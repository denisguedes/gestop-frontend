import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SegurancaGuard } from 'src/app/seguranca/seguranca.guard';
import { FormComponent } from './form/form.component';
import { ListComponent } from './list/list.component';

const routes: Routes = [
    {
      path: 'gestop/pais/novo',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['PAIS_CADASTRAR']}
    },
    {
      path: 'gestop/pais/listar',
      component: ListComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['PAIS_CONSULTAR']}
    },
    {
      path: 'gestop/pais/:codigo',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['PAIS_ALTERAR']}
    },
    {
      path: 'gestop/pais/:codigo/:consulta',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['PAIS_CONSULTAR']}
    },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})

export class PaisRoutingModule { }
