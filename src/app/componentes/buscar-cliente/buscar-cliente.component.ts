import { Cliente } from 'src/app/administrador/cliente/cliente';
import { ErrorHandlerService } from './../../core/error-handler.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { LazyLoadEvent } from 'primeng/api';

import { ClienteFiltro, ClienteService } from 'src/app/administrador/cliente/cliente.service';

@Component({
  selector: 'buscar-cliente',
  templateUrl: './buscar-cliente.component.html',
  styleUrls: ['./buscar-cliente.component.css']
})
export class BuscarClienteComponent implements OnInit {

  @Output() seleciona = new EventEmitter();

  private visibleDialogEntidade: boolean;
  private selecionados: Cliente[];
  private filtro = new ClienteFiltro;
  private totalRegistros = 0;
  private entidadeSelecionada = new Cliente();
  private codigoSelecionado: number;

  constructor(
    private entidadeService: ClienteService,
    private errorHandler : ErrorHandlerService,
  ) { }

  ngOnInit() {
  }

  showDialogEntidade(){
    this.limparFiltro ();
    this.selecionados = [];
    this.visibleDialogEntidade = true;
  }

  pesquisarEntidade(pagina = 0) {
    this.filtro.pagina = pagina;
    this.filtro.itensPorPagina =  10;

    this.entidadeService.pesquisar(this.filtro)
      .then(resultado => {
        this.totalRegistros = resultado.total;
        this.selecionados = resultado.selecionados;
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  aoMudarPagina (event: LazyLoadEvent) {
    const pagina = event.first / event.rows;
    this.pesquisarEntidade(pagina);
  }

  selecionarEntidade(fornecedor){
    this.codigoSelecionado = fornecedor.codigo;
    this.entidadeSelecionada = fornecedor;
  }

  limparFiltro () {
    this.filtro = new ClienteFiltro();
    this.totalRegistros = 0;
    this.selecionados = [];
  }

  confirmarModal(entidade){
    if(entidade){
      this.seleciona.emit(entidade);
    }
    this.limparFiltro ();
    this.selecionados = [];
    this.visibleDialogEntidade = false;
  }

  cancelarModal(){
    this.limparFiltro ();
    this.selecionados = [];
    this.visibleDialogEntidade = false;
  }

}
