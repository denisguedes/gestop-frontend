import { EstadoService, EstadoFiltro } from './../../cadastro/estado/estado.service';
import { Estado } from './../../cadastro/estado/estado';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { LazyLoadEvent } from 'primeng/api';

import { ErrorHandlerService } from 'src/app/core/error-handler.service';

@Component({
  selector: 'buscar-estado',
  templateUrl: './buscar-estado.component.html',
  styleUrls: ['./buscar-estado.component.css']
})
export class BuscarEstadoComponent implements OnInit {

  @Output() seleciona = new EventEmitter();

  private visibleDialogEntidade: boolean;
  private selecionados: Estado[];
  private filtro = new EstadoFiltro;
  private totalRegistros = 0;
  private entidadeSelecionada = new Estado();
  private codigoSelecionado: number;

  constructor(
    private entidadeService: EstadoService,
    private errorHandler : ErrorHandlerService,
  ) { }

  ngOnInit() {
  }

  showDialogEntidade(){
    this.limparFiltro ();
    this.selecionados = [];
    this.visibleDialogEntidade = true;
  }

  pesquisarEntidade(pagina = 0) {
    this.filtro.pagina = pagina;
    this.filtro.itensPorPagina =  10;

    this.entidadeService.pesquisar(this.filtro)
      .then(resultado => {
        this.totalRegistros = resultado.total;
        this.selecionados = resultado.selecionados;
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  aoMudarPagina (event: LazyLoadEvent) {
    const pagina = event.first / event.rows;
    this.pesquisarEntidade(pagina);
  }

  selecionarEntidade(fornecedor){
    this.codigoSelecionado = fornecedor.codigo;
    this.entidadeSelecionada = fornecedor;
  }

  limparFiltro () {
    this.filtro = new EstadoFiltro();
    this.totalRegistros = 0;
    this.selecionados = [];
  }

  confirmarModal(entidade){
    if(entidade){
      this.seleciona.emit(entidade);
    }
    this.limparFiltro ();
    this.selecionados = [];
    this.visibleDialogEntidade = false;
  }

  cancelarModal(){
    this.limparFiltro ();
    this.selecionados = [];
    this.visibleDialogEntidade = false;
  }

}
