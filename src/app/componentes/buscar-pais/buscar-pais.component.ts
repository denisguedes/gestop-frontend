import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { LazyLoadEvent } from 'primeng/api';

import { ErrorHandlerService } from 'src/app/core/error-handler.service';
import { Pais } from 'src/app/cadastro/pais/pais';
import { PaisFiltro, PaisService } from 'src/app/cadastro/pais/pais.service';


@Component({
  selector: 'buscar-pais',
  templateUrl: './buscar-pais.component.html',
  styleUrls: ['./buscar-pais.component.css']
})
export class BuscarPaisComponent implements OnInit {

  @Output() seleciona = new EventEmitter();

  private visibleDialogEntidade: boolean;
  private selecionados: Pais[];
  private filtro = new PaisFiltro;
  private totalRegistros = 0;
  private entidadeSelecionada = new Pais();
  private codigoSelecionado: number;

  constructor(
    private entidadeService: PaisService,
    private errorHandler : ErrorHandlerService,
  ) { }

  ngOnInit() {
  }


  showDialogEntidade(){
    this.limparFiltro ();
    this.selecionados = [];
    this.visibleDialogEntidade = true;
  }

  pesquisarEntidade(pagina = 0) {
    this.filtro.pagina = pagina;
    this.filtro.itensPorPagina =  10;

    this.entidadeService.pesquisar(this.filtro)
      .then(resultado => {
        this.totalRegistros = resultado.total;
        this.selecionados = resultado.selecionados;
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  aoMudarPagina (event: LazyLoadEvent) {
    const pagina = event.first / event.rows;
    this.pesquisarEntidade(pagina);
  }

  selecionarEntidade(fornecedor){
    this.codigoSelecionado = fornecedor.codigo;
    this.entidadeSelecionada = fornecedor;
  }

  limparFiltro () {
    this.filtro = new PaisFiltro();
    this.totalRegistros = 0;
    this.selecionados = [];
  }

  confirmarModal(entidade){
    if(entidade){
      this.seleciona.emit(entidade);
    }
    this.limparFiltro ();
    this.selecionados = [];
    this.visibleDialogEntidade = false;
  }

  cancelarModal(){
    this.limparFiltro ();
    this.selecionados = [];
    this.visibleDialogEntidade = false;
  }

}
