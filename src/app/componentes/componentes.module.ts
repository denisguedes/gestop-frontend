import { BuscarProdutoComponent } from './buscar-produto/buscar-produto.component';
import { BuscarFornecedorComponent } from './buscar-fornecedor/buscar-fornecedor.component';
import { BuscarCategoriaComponent } from './buscar-categoria/buscar-categoria.component';
import { BuscarPerfilComponent } from './buscar-perfil/buscar-perfil.component';
import { BuscarCidadeComponent } from './buscar-cidade/buscar-cidade.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import {InputTextModule} from 'primeng/inputtext';
import {ButtonModule} from 'primeng/button';
import {TableModule} from 'primeng/table';
import {TooltipModule} from 'primeng/tooltip';
import {FieldsetModule} from 'primeng/fieldset';
import {BreadcrumbModule} from 'primeng/breadcrumb';
import {RadioButtonModule} from 'primeng/radiobutton';
import {DropdownModule} from 'primeng/dropdown';
import {DialogModule} from 'primeng/dialog';
import {CheckboxModule} from 'primeng/checkbox';
import {ScrollPanelModule} from 'primeng/scrollpanel';

import { MenuFormComponent } from './menu-form/menu-form.component';
import { MenuListComponent } from './menu-list/menu-list.component';
import { BuscarPaisComponent } from './buscar-pais/buscar-pais.component';
import { BuscarEstadoComponent } from './buscar-estado/buscar-estado.component';
import { BuscarClienteComponent } from './buscar-cliente/buscar-cliente.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,

    InputTextModule,
    ButtonModule,
    TableModule,
    TooltipModule,
    FieldsetModule,
    BreadcrumbModule,
    RadioButtonModule,
    DropdownModule,
    DialogModule,
    CheckboxModule,
    ScrollPanelModule
  ],
  declarations: [
    MenuFormComponent,
    MenuListComponent,

    BuscarPaisComponent,
    BuscarPaisComponent,
    BuscarEstadoComponent,
    BuscarCidadeComponent,
    BuscarPerfilComponent,
    BuscarCategoriaComponent,
    BuscarFornecedorComponent,
    BuscarProdutoComponent,
    BuscarClienteComponent
  ],
  exports: [
    MenuFormComponent,
    MenuListComponent,

    BuscarPaisComponent,
    BuscarPaisComponent,
    BuscarEstadoComponent,
    BuscarCidadeComponent,
    BuscarPerfilComponent,
    BuscarCategoriaComponent,
    BuscarFornecedorComponent,
    BuscarProdutoComponent,
    BuscarClienteComponent
  ]
})
export class ComponentesModule { }
