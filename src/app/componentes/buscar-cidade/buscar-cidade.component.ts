import { CidadeFiltro, CidadeService } from './../../cadastro/cidade/cidade.service';
import { Cidade } from './../../cadastro/cidade/cidade';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { LazyLoadEvent } from 'primeng/api';

import { ErrorHandlerService } from 'src/app/core/error-handler.service';

@Component({
  selector: 'buscar-cidade',
  templateUrl: './buscar-cidade.component.html',
  styleUrls: ['./buscar-cidade.component.css']
})
export class BuscarCidadeComponent implements OnInit {

  @Output() seleciona = new EventEmitter();

  private visibleDialogEntidade: boolean;
  private selecionados: Cidade[];
  private filtro = new CidadeFiltro;
  private totalRegistros = 0;
  private entidadeSelecionada = new Cidade();
  private codigoSelecionado: number;

  constructor(
    private entidadeService: CidadeService,
    private errorHandler : ErrorHandlerService,
  ) { }

  ngOnInit() {
  }


  showDialogEntidade(){
    this.limparFiltro ();
    this.selecionados = [];
    this.visibleDialogEntidade = true;
  }

  pesquisarEntidade(pagina = 0) {
    this.filtro.pagina = pagina;
    this.filtro.itensPorPagina =  10;

    this.entidadeService.pesquisar(this.filtro)
      .then(resultado => {
        this.totalRegistros = resultado.total;
        this.selecionados = resultado.selecionados;
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  aoMudarPagina (event: LazyLoadEvent) {
    const pagina = event.first / event.rows;
    this.pesquisarEntidade(pagina);
  }

  selecionarEntidade(fornecedor){
    this.codigoSelecionado = fornecedor.codigo;
    this.entidadeSelecionada = fornecedor;
  }

  limparFiltro () {
    this.filtro = new CidadeFiltro();
    this.totalRegistros = 0;
    this.selecionados = [];
  }

  confirmarModal(entidade){
    if(entidade){
      this.seleciona.emit(entidade);
    }
    this.limparFiltro ();
    this.selecionados = [];
    this.visibleDialogEntidade = false;
  }

  cancelarModal(){
    this.limparFiltro ();
    this.selecionados = [];
    this.visibleDialogEntidade = false;
  }

}
