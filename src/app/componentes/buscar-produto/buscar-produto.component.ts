import { Produto } from 'src/app/administrador/produto/produto';
import { ErrorHandlerService } from './../../core/error-handler.service';
import { CategoriaFiltro, CategoriaService } from './../../administrador/categoria/categoria.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { LazyLoadEvent } from 'primeng/api';
import { Categoria } from '../../administrador/categoria/categoria';
import { ProdutoFiltro, ProdutoService } from 'src/app/administrador/produto/produto.service';
import { element } from '@angular/core/src/render3';

@Component({
  selector: 'buscar-produto',
  templateUrl: './buscar-produto.component.html',
  styleUrls: ['./buscar-produto.component.css']
})
export class BuscarProdutoComponent implements OnInit {

  @Output() seleciona = new EventEmitter();

  private visibleDialogEntidade: boolean;
  private selecionados: Produto[] = [];
  private filtro = new ProdutoFiltro;
  private totalRegistros = 0;
  private entidadeSelecionada: Produto[] =  [];
  private codigoSelecionado: Produto[];

  constructor(
    private entidadeService: ProdutoService,
    private errorHandler : ErrorHandlerService,
  ) { }

  ngOnInit() {
  }

  showDialogEntidade(){
    this.limparFiltro ();
    this.selecionados = [];
    this.visibleDialogEntidade = true;
  }

  pesquisarEntidade(pagina = 0) {
    this.filtro.pagina = pagina;
    this.filtro.itensPorPagina =  10;

    this.entidadeService.pesquisar(this.filtro)
      .then(resultado => {
        this.totalRegistros = resultado.total;
        resultado.selecionados.forEach(element => {
          element.quantidade = 1;
          this.selecionados.push(element);
        });
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  aoMudarPagina (event: LazyLoadEvent) {
    const pagina = event.first / event.rows;
    this.pesquisarEntidade(pagina);
  }

  selecionarEntidade(fornecedor){
    this.entidadeSelecionada = [];

    this.codigoSelecionado.push(fornecedor);
    this.codigoSelecionado.forEach(element => {
      if(element.codigo !== undefined){
        this.entidadeSelecionada.push(element);
      }
    });
  }

  limparFiltro () {
    this.filtro = new ProdutoFiltro();
    this.totalRegistros = 0;
    this.selecionados = [];
  }

  confirmarModal(entidade){
    this.codigoSelecionado = [];
    if(entidade){
      this.seleciona.emit(entidade);
    }
    this.limparFiltro ();
    this.selecionados = [];
    this.entidadeSelecionada = [];
    this.visibleDialogEntidade = false;
  }

  cancelarModal(){
    this.limparFiltro ();
    this.selecionados = [];
    this.visibleDialogEntidade = false;
  }

}
