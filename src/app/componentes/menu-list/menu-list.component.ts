import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-menu-list',
  templateUrl: './menu-list.component.html',
  styleUrls: ['./menu-list.component.css']
})
export class MenuListComponent {

  @Input() rotaNovo: string;
  @Input() rotaPadrao: string;
  @Input() codigoEntidade: number;
  @Input() consultar = true;
  @Output() excluir = new EventEmitter();
  @Output() relatorio = new EventEmitter();
  @Input() permissaoNovo: boolean = true;
  @Input() permissaoEditar: boolean = true;
  @Input() permissaoConsultar: boolean = true;
  @Input() permissaoExcluir: boolean = true;
  @Input() permissaoRelatorio: boolean = true;

  constructor(
    private router: Router,
    private toastr: ToastrService,
  ) { }

  existeItemSelecionado (tipo: string) {
    if(!this.codigoEntidade){
      this.toastr.warning('Selecione um item da lista!');
    }else{
      if (tipo === 'editar') {
        this.router.navigate([this.rotaPadrao + this.codigoEntidade]);
      }
      if (tipo === 'consultar') {
        this.router.navigate([this.rotaPadrao + this.codigoEntidade+'/'+this.consultar]);
      }
    }
  }

  excluirEntidade(codigo:number){
    this.excluir.emit(codigo);
  }
}
