import { ErrorHandlerService } from './../../core/error-handler.service';
import { CategoriaFiltro, CategoriaService } from './../../administrador/categoria/categoria.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { LazyLoadEvent } from 'primeng/api';
import { Categoria } from '../../administrador/categoria/categoria';

@Component({
  selector: 'buscar-categoria',
  templateUrl: './buscar-categoria.component.html',
  styleUrls: ['./buscar-categoria.component.css']
})
export class BuscarCategoriaComponent implements OnInit {

  @Output() seleciona = new EventEmitter();

  private visibleDialogEntidade: boolean;
  private selecionados: Categoria[];
  private filtro = new CategoriaFiltro;
  private totalRegistros = 0;
  private entidadeSelecionada = new Categoria();
  private codigoSelecionado: number;

  constructor(
    private entidadeService: CategoriaService,
    private errorHandler : ErrorHandlerService,
  ) { }

  ngOnInit() {
  }

  showDialogEntidade(){
    this.limparFiltro ();
    this.selecionados = [];
    this.visibleDialogEntidade = true;
  }

  pesquisarEntidade(pagina = 0) {
    this.filtro.pagina = pagina;
    this.filtro.itensPorPagina =  10;

    this.entidadeService.pesquisar(this.filtro)
      .then(resultado => {
        this.totalRegistros = resultado.total;
        this.selecionados = resultado.selecionados;
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  aoMudarPagina (event: LazyLoadEvent) {
    const pagina = event.first / event.rows;
    this.pesquisarEntidade(pagina);
  }

  selecionarEntidade(fornecedor){
    this.codigoSelecionado = fornecedor.codigo;
    this.entidadeSelecionada = fornecedor;
  }

  limparFiltro () {
    this.filtro = new CategoriaFiltro();
    this.totalRegistros = 0;
    this.selecionados = [];
  }

  confirmarModal(entidade){
    if(entidade){
      this.seleciona.emit(entidade);
    }
    this.limparFiltro ();
    this.selecionados = [];
    this.visibleDialogEntidade = false;
  }

  cancelarModal(){
    this.limparFiltro ();
    this.selecionados = [];
    this.visibleDialogEntidade = false;
  }

}
