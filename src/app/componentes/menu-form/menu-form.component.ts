import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { ConfirmationService } from 'primeng/components/common/api';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-menu-form',
  templateUrl: './menu-form.component.html',
  styleUrls: ['./menu-form.component.css']
})
export class MenuFormComponent {

  @Input() rotaListar: string;
  @Input() entidade: string;
  @Input() consulta: boolean;
  @Input() formulario: FormControl;
  @Output() salvar = new EventEmitter();

  constructor(
    private router: Router,
    private confirmation: ConfirmationService,
  ) { }

  cancelar (){

    let msg = "";
    if(this.entidade['codigo']){
      msg = "Tem certeza que deseja descartar a alteração";
    }else{
      msg = "Tem certeza que deseja descartar a inclusão";
    }

    this.confirmation.confirm({
      message: msg,
      accept: (() => {            
        this.router.navigate([this.rotaListar]);        
      })
    });
  }

  salvarEntidade(formulario: FormControl){    
    this.salvar.emit(formulario);
  }

}
