import { CargoModule } from './administrador/cargo/cargo.module';
import { PedidoModule } from './financeiro/pedido/pedido.module';
import { FornecedorModule } from './administrador/fornecedor/fornecedor.module';
import { ProdutoModule } from './administrador/produto/produto.module';
import { ClienteModule } from './administrador/cliente/cliente.module';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PaginaNaoEncontradaComponent } from './core/pagina-nao-encontrada/pagina-nao-encontrada.component';
import { PaginaPrincipalComponent } from './core/pagina-principal/pagina-principal.component';
import { AcessoNaoAutorizadoComponent } from './core/acesso-nao-autorizado/acesso-nao-autorizado.component';
import { SegurancaGuard } from './seguranca/seguranca.guard';

import { SegurancaModule } from './seguranca/seguranca.module';
import { PaisModule } from './cadastro/pais/pais.module';
import { EstadoModule } from './cadastro/estado/estado.module';
import { CidadeModule } from './cadastro/cidade/cidade.module';
import { PermissaoModule } from './seguranca/permissao/permissao.module';
import { PerfilModule } from './seguranca/perfil/perfil.module';
import { UsuarioModule } from './seguranca/usuario/usuario.module';
import { CategoriaModule } from './administrador/categoria/categoria.module';
import { FormaPagamentoModule } from './financeiro/forma-pagamento/forma-pagamento.module';
import { PagamentoModule } from './financeiro/pagamento/pagamento.module';
import { FuncioarioModule } from './administrador/funcionario/funcionario.module';
import { RelatorioModule } from './relatorios/relatorio.module';

const routes: Routes = [
  {
    path: '',
    redirectTo:'gestop/principal',
    pathMatch: 'full'
  },
  {
    path: 'gestop/principal',
    component: PaginaPrincipalComponent,
    canActivate:[SegurancaGuard],
  },
  {
    path: 'gestop/acesso-nao-autorizado',
    component: AcessoNaoAutorizadoComponent,
  },
  {
    path: '**',
    redirectTo: 'pagina-nao-encontrada'
  },
  {
    path: 'pagina-nao-encontrada',
    component: PaginaNaoEncontradaComponent
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    SegurancaModule,
    PaisModule,
    EstadoModule,
    PermissaoModule,
    PerfilModule,
    CidadeModule,
    UsuarioModule,
    ClienteModule,
    CategoriaModule,
    ProdutoModule,
    FornecedorModule,
    PedidoModule,
    PagamentoModule,
    FormaPagamentoModule,
    CargoModule,
    FuncioarioModule,
    RelatorioModule
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
