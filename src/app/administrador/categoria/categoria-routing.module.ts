import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SegurancaGuard } from 'src/app/seguranca/seguranca.guard';
import { FormComponent } from './form/form.component';
import { ListComponent } from './list/list.component';

const routes: Routes = [
    {
      path: 'gestop/categoria/novo',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['CATEGORIA_CADASTRAR']}
    },
    {
      path: 'gestop/categoria/listar',
      component: ListComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['CATEGORIA_CONSULTAR']}
    },
    {
      path: 'gestop/categoria/:codigo',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['CATEGORIA_ALTERAR']}
    },
    {
      path: 'gestop/categoria/:codigo/:consulta',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['CATEGORIA_CONSULTAR']}
    },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})

export class CategoriaRoutingModule { }
