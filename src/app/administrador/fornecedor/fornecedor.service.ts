import { Fornecedor } from './fornecedor';
import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

import { environment } from 'src/environments/environment';
import { SistemaHttp } from 'src/app/seguranca/sistema-http';
import { CrudService } from 'src/app/core/crud-service';

export class FornecedorFiltro {
  codigo: number;
  razaoSocial: string;
  pagina = 0;
  itensPorPagina: number;
}

@Injectable({
  providedIn: 'root'
})
export class FornecedorService extends CrudService<Fornecedor>{

  private baseUrl = `${environment.apiUrl}/gestop-api/fornecedor`;

  constructor(protected http: SistemaHttp) {
    super(http, `${environment.apiUrl}/gestop-api/fornecedor`);
  }

  pesquisar(filtro: FornecedorFiltro): Promise<any> {

    let params = new HttpParams({
      fromObject: {
        page: filtro.pagina.toString(),
        size: filtro.itensPorPagina.toString()
      }
    });

    if(filtro.codigo) {
      params = params.append('codigo', filtro.codigo.toString());
    }

    if(filtro.razaoSocial) {
      params = params.append('razaoSocial', filtro.razaoSocial);
    }

    return this.http.get<any>(this.baseUrl, { params })
      .toPromise()
      .then( response => {
        const resultado = {
          selecionados: response.content,
          total: response.totalElements
         }
        return resultado;
      });
  }

}
