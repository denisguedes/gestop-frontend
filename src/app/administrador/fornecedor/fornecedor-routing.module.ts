
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SegurancaGuard } from 'src/app/seguranca/seguranca.guard';
import { FormComponent } from './form/form.component';
import { ListComponent } from './list/list.component';

const routes: Routes = [
    {
      path: 'gestop/fornecedor/novo',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['FORNECEDOR_CADASTRAR']}
    },
    {
      path: 'gestop/fornecedor/listar',
      component: ListComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['FORNECEDOR_CONSULTAR']}
    },
    {
      path: 'gestop/fornecedor/:codigo',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['FORNECEDOR_ALTERAR']}
    },
    {
      path: 'gestop/fornecedor/:codigo/:consulta',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['FORNECEDOR_CONSULTAR']}
    },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class FornecedorRoutingModule { }
