import { Contato } from '../model/contato';
import { Endereco } from '../model/endereco';

export class Fornecedor {
  codigo: number;
  razaoSocial: string;
  nomeFantasia: string;
  cpfCnpj: string;
  tipo: any;
  contatos = new Array<Contato>();
  endereco = new Endereco();

  constructor(codigo?: number, razaoSocial?: string, nomeFantasia?: string, cpfCnpj?: string){
    this.codigo = codigo;
    this.razaoSocial = razaoSocial;
    this.nomeFantasia = nomeFantasia;
    this.cpfCnpj = cpfCnpj;
  }
}
