import { Cargo } from './../cargo/cargo';
import { Contato } from '../model/contato';
import { Endereco } from '../model/endereco';

export class Funcionario {
  codigo: number;
  nome: string;
  cpf: string;
  cargo = new Cargo();
  contatos = new Array<Contato>();
  endereco = new Endereco();

  constructor(codigo?: number, nome?: string){
    this.codigo = codigo;
    this.nome = nome;
    this.cargo = new Cargo();
    this.endereco = new Endereco();
  }
}


