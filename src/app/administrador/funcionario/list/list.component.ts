import { ConfirmationService, LazyLoadEvent } from 'primeng/components/common/api';
import { ErrorHandlerService } from 'src/app/core/error-handler.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { Title } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { SegurancaService } from 'src/app/seguranca/seguranca.service';
import { FuncionarioFiltro, FuncionarioService } from '../funcionario.service';
import { Funcionario } from '../funcionario';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  private items: MenuItem[];
  private quantidadeItemPorPagina: any[];
  private quantidadeItemPorPaginaSelecionada: any;
  private selecionados: Funcionario[];
  private filtro = new FuncionarioFiltro;
  private totalRegistros = 0;
  private codigoEntidade: number;
  private consultar = true;
  private rotaNovo = "/gestop/funcionario/novo";
  private rotaPadrao = '/gestop/funcionario/';
  @ViewChild('tabela') tabelaEntidade;

  constructor(
    private entidadeService: FuncionarioService,
    private title: Title,
    private toastr: ToastrService,
    private errorHandler: ErrorHandlerService,
    private confirmation: ConfirmationService,
    private router: Router,
    private segurancaService: SegurancaService
  ) {}

  ngOnInit() {

    this.pesquisar();

    this.items = [
      {label:'Gestop'},
      {label:'Cadastro'},
      {label:'Listagem de Funcionário'}
    ];

    this.quantidadeItemPorPagina = [
      {value:10},
      {value:20},
      {value:50},
    ]

    this.title.setTitle ('Gestop - Funcionário');
  }

  pesquisar(pagina = 0) {
    this.filtro.pagina = pagina;
    this.filtro.itensPorPagina = this.quantidadeItemPorPaginaSelecionada ? this.quantidadeItemPorPaginaSelecionada.value : 10;

    this.entidadeService.pesquisar(this.filtro)
      .then(resultado => {
        this.totalRegistros = resultado.total;
        this.selecionados = resultado.selecionados;
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  excluir (codigo:number) {
    if(!this.codigoEntidade){
      this.toastr.warning('Selecione um item da lista!');
    }else{
      this.confirmation.confirm({
        message: 'Tem certeza que deseja excluir',
        accept: (() => {
            this.entidadeService.excluir(codigo)
              .then(() => {
                if(this.tabelaEntidade.first === 0){
                  this.pesquisar();
                }else {
                  this.tabelaEntidade.first = 0;
                }
                this.toastr.success('Item excluído com sucesso!');
              })
              .catch (erro => this.errorHandler.handle(erro));
        })
      });
    }
  }

  limparFiltro () {
    this.filtro = new FuncionarioFiltro();
    this.pesquisar();
  }

  aoMudarPagina (event: LazyLoadEvent) {
    const pagina = event.first / event.rows;
    this.pesquisar(pagina);
  }

  selecionarEntidade (entidade){
    this.codigoEntidade = entidade.codigo;
  }

}
