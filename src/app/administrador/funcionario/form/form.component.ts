import { CargoService } from './../../cargo/cargo.service';
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { MenuItem, ConfirmationService } from 'primeng/components/common/api';
import { ToastrService } from 'ngx-toastr';
import { ErrorHandlerService } from 'src/app/core/error-handler.service';
import { DataLocaleService } from 'src/app/core/data-locale-service';
import { Funcionario } from '../funcionario';
import { FuncionarioService } from '../funcionario.service';
import { Cargo } from '../../cargo/cargo';
import { Contato } from '../../model/contato';
import { Endereco } from '../../model/endereco';
import { Cidade } from 'src/app/cadastro/cidade/cidade';
import { Estado } from 'src/app/cadastro/estado/estado';
import { EstadoEnum } from 'src/app/core/estado-enum';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  private items: MenuItem[];
  private entidade = new Funcionario();
  private consulta: boolean;
  private codigoEntidade: number;
  private rotaListar = "/gestop/funcionario/listar";
  private contatosSelecionados: Contato[] = [];
  private contato = new Contato();
  private visibleDialogContato: boolean;
  private indexContato: number;
  private editandoContato: boolean;
  private cargos: Cargo[] = [];
  private valorTotalPedido = 0;

  //Campos para validação
  private nome = true;
  private cidade = true;
  private tipo = true;
  private status = true;
  private cpf = true;
  private endereco = true;
  private email = true;
  private telefone = true;
  private dataVencimento = true;
  private cargo = true;
  private estados = new EstadoEnum();

  constructor(
    private entidadeService: FuncionarioService,
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private confirmation: ConfirmationService,
    private title: Title,
    private errorHandler : ErrorHandlerService,
    private cargoService: CargoService,
    private locale: DataLocaleService,
  ) { }

  ngOnInit() {
    this.items = [
      {label:'Gestop'},
      {label:'Cadastro'},
      {label:'Funcionário'}
    ];
    this.locale;
    this.valorTotalPedido;
    this.codigoEntidade = this.route.snapshot.params['codigo'];
    this.consulta = this.route.snapshot.params['consulta'];
    this.buscarCargos();
    if(this.codigoEntidade){
      this.buscar(this.codigoEntidade);
    }
  }

  buscarCargos(){
    this.cargoService.listar().then( element => {
      this.cargos = element.map(c => ({ label: c.nome, value: c.codigo }));
    })
    .catch(erro => {
      this.errorHandler.handle(erro);
    });
  }

  incluir (form: FormControl) {
    this.entidadeService.incluir(this.entidade)
      .then( ()=>{
        this.toastr.success('Cadastro realizado com sucesso');
        this.limparFormulario(form);
        this.router.navigate([this.rotaListar]);
      })
      .catch(erro => {
        this.errorHandler.handle(erro);
        this.exibiErroFormulario(form);
      });
  }

  alterar (form: FormControl) {
    this.entidadeService.alterar(this.entidade)
      .then( dados => {
        this.entidade = dados;
        this.toastr.success('Alteração realizada com sucesso');
        this.router.navigate([this.rotaListar]);
      })
      .catch(erro => {
        this.errorHandler.handle(erro)
        this.exibiErroFormulario(form);
      });
  }

  buscar (codigo: number) {
    this.entidadeService.buscar(codigo)
      .then( dados => {
        this.entidade = dados;
        if(!this.entidade.endereco){
          this.entidade.endereco = new Endereco();
        }
        this.preencherContatosSalvo();
        if(this.consulta){
          this.title.setTitle (`Gestop - Consulta de Funcionário:${this.entidade.nome}`);
        }else{
          this.title.setTitle (`Gestop - Alteração de Funcionário :${this.entidade.nome}`);
        }
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  salvar (form: FormControl) {

    this.preencherContatosParaSalvar();
    if (this.entidade.codigo){
      this.alterar(form);
    } else {
      this.incluir(form);
    }
  }

  limparFormulario (form: FormControl){
    this.entidade = new Funcionario();
    form.reset();
  }

  exibiErroFormulario(form: FormControl){

    if(!form.value.nome) {
      this.nome = false;
    }else {
      this.nome = true;
    }

    if(!form.value.tipo) {
      this.tipo = false;
    }else {
      this.tipo = true;
    }

    if(!form.value.cpf) {
      this.cpf = false;
    }else {
      this.cpf = true;
    }

    if(!form.value.endereco) {
      this.endereco = false;
    }else {
      this.endereco = true;
    }

    if(!form.value.dataVencimento) {
      this.dataVencimento = false;
    }else {
      this.dataVencimento = true;
    }

    if(!form.value.cargo) {
      this.cargo = false;
    }else {
      this.cargo = true;
    }
  }

  selecionarCidade (cidade){
    this.entidade.endereco.cidade = cidade;
  }

  selecionarEstado (estado){
    this.entidade.endereco.estado = estado;
  }

  salvarContato(contato: Contato) {

    if(contato.email || contato.telefone){

      if(this.editandoContato){
        this.contatosSelecionados[this.indexContato] = new Contato(contato.codigo, contato.email, contato.telefone);
      }else{
        this.contatosSelecionados.push(contato);
      }
      this.contato = new Contato();
      this.visibleDialogContato = false;
      this.editandoContato = false;

    }else{
        this.toastr.error('Informe o telefone ou e-mail!');
        this.email = false;
        this.telefone = false;
    }
  }

  editarContato(contato: Contato, index: number) {
    this.contato = new Contato(contato.codigo, contato.email, contato.telefone);
    this.indexContato = index;
    this.editandoContato = true
    this.visibleDialogContato = true;
  }

  removerContato(index: number) {
    this.confirmation.confirm({
      message: 'Tem certeza que deseja excluir',
      accept: (() => {
        this.contatosSelecionados.splice(index, 1);
        this.toastr.success('Item excluído com sucesso!');
      })
    });
  }

  showDialogContato(){
    this.visibleDialogContato = true;
  }

  cancelarModal(){
    this.visibleDialogContato = false;
  }

  preencherContatosParaSalvar(){
    this.entidade.contatos = new Array<Contato>();

    for(let i = 0; i < this.contatosSelecionados.length; i++) {
      let contato: Contato = this.contatosSelecionados[i];
      this.entidade.contatos.push(new Contato(contato.codigo, contato.email, contato.telefone));
    }
  }

  preencherContatosSalvo() {
    for(let i = 0; i < this.entidade.contatos.length; i++) {
      let contato = this.entidade.contatos[i];
      this.contatosSelecionados.push(contato);
    }
  }

}
