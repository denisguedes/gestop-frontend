import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SegurancaGuard } from 'src/app/seguranca/seguranca.guard';
import { FormComponent } from './form/form.component';
import { ListComponent } from './list/list.component';

const routes: Routes = [
    {
      path: 'gestop/produto/novo',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['PRODUTO_CADASTRAR']}
    },
    {
      path: 'gestop/produto/listar',
      component: ListComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['PRODUTO_CONSULTAR']}
    },
    {
      path: 'gestop/produto/:codigo',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['PRODUTO_ALTERAR']}
    },
    {
      path: 'gestop/produto/:codigo/:consulta',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['PRODUTO_CONSULTAR']}
    },
    {
      path: 'gestop/relatorio/produto/:codigo/:consulta',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['PRODUTO_RELATORIO']}
    },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class ProdutoRoutingModule { }
