import { Fornecedor } from './../fornecedor/fornecedor';
import { Categoria } from './../categoria/categoria';

export class Produto {
    codigo: number;
    descricao: string;
    codigoProprio: string;
    categoria = new Categoria();
    valorCusto: number;
    valorVenda: number;
    observacao: string;
    qtdAtual: number;
    quantidade: number = 1;
    qtdMinima: number;
    valorDesconto: number;
    valorMargem: number;
    embalagem: number;
    percentual: number;
    fornecedor = new Fornecedor();

    constructor(codigo?: number, descricao?: string, codigoProprio?: string, valorVenda?: number){
      this.codigo = codigo;
      this.descricao = descricao;
      this.codigoProprio = codigoProprio;
      this.valorVenda = valorVenda;
    }
}

export class Movimentacao {
  data: Date;
}

