import { InputMaskModule } from 'primeng/inputmask';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import {InputTextModule} from 'primeng/inputtext';
import {ButtonModule} from 'primeng/button';
import {TableModule} from 'primeng/table';
import {TooltipModule} from 'primeng/tooltip';
import {FieldsetModule} from 'primeng/fieldset';
import {BreadcrumbModule} from 'primeng/breadcrumb';
import {RadioButtonModule} from 'primeng/radiobutton';
import {DropdownModule} from 'primeng/dropdown';
import {CurrencyMaskModule} from "ng2-currency-mask";

import { ComponentesModule } from 'src/app/componentes/componentes.module';
import { FormComponent } from './form/form.component';
import { ListComponent } from './list/list.component';
import { ProdutoRoutingModule } from './produto-routing.module';
import { DialogModule } from 'primeng/dialog';
import { TabViewModule } from 'primeng/tabview';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { RelatorioComponent } from './relatorio/relatorio.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,

    ComponentesModule,
    ProdutoRoutingModule,

    InputTextModule,
    ButtonModule,
    TableModule,
    TooltipModule,
    FieldsetModule,
    BreadcrumbModule,
    RadioButtonModule,
    DropdownModule,
    TabViewModule,
    DialogModule,
    InputMaskModule,
    InputTextareaModule,
    CurrencyMaskModule
  ],
  declarations: [
    FormComponent,
    ListComponent,
    RelatorioComponent
  ]
})
export class ProdutoModule { }
