import { ProdutoService, ProdutoFiltro } from './../produto.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-relatorio',
  templateUrl: './relatorio.component.html',
  styleUrls: ['./relatorio.component.css']
})
export class RelatorioComponent implements OnInit {

  private filtro = new ProdutoFiltro

  constructor(
    private produtoService: ProdutoService) { }

  ngOnInit() {
  }

  gerar() {
    this.produtoService.gerarRelatorio(null)
      .then(relatorio => {
        const url = window.URL.createObjectURL(relatorio);

        window.open(url);
      });
  }
}
