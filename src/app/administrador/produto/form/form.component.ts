import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { MenuItem } from 'primeng/components/common/api';
import { ToastrService } from 'ngx-toastr';
import { ErrorHandlerService } from 'src/app/core/error-handler.service';
import { Produto } from '../produto';
import { ProdutoService } from '../produto.service';
import { Fornecedor } from '../../fornecedor/fornecedor';


@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  private items: MenuItem[];
  private entidade = new Produto();
  private consulta: boolean;
  private codigoEntidade: number;
  private rotaListar = "/gestop/produto/listar";

  //Campos para validação
  private descricao = true;
  private codigoProprio = true;
  private valorCusto = true;
  private valorVenda = true;
  private observacao = true;
  private qtdAtual = true;
  private qtdMinima = true;
  private valorDesconto = true;
  private valorMargem = true;
  private embalagem = true;
  private percentual = true;
  private categoria = true;
  private fornecedor = true;

  constructor(
    private entidadeService: ProdutoService,
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private title: Title,
    private errorHandler : ErrorHandlerService,
  ) { }

  ngOnInit() {

    this.items = [
      {label:'Gestop'},
      {label:'Cadastro'},
      {label:'Produto'}
    ];

    this.codigoEntidade = this.route.snapshot.params['codigo'];
    this.consulta = this.route.snapshot.params['consulta'];

    if(this.codigoEntidade){
      this.buscar(this.codigoEntidade);
    }

  }

  incluir (form: FormControl) {
    this.entidadeService.incluir(this.entidade)
      .then( ()=>{
        this.toastr.success('Cadastro realizado com sucesso');
        this.limparFormulario(form);
        this.router.navigate([this.rotaListar]);
      })
      .catch(erro => {
        this.errorHandler.handle(erro);
        this.exibiErroFormulario(form);
      });
  }

  alterar (form: FormControl) {
    this.entidadeService.alterar(this.entidade)
      .then( dados => {
        this.entidade = dados;
        this.toastr.success('Alteração realizada com sucesso');
        this.router.navigate([this.rotaListar]);
      })
      .catch(erro => {
        this.errorHandler.handle(erro)
        this.exibiErroFormulario(form);
      });
  }

  buscar (codigo: number) {
    this.entidadeService.buscar(codigo)
      .then( dados => {
        this.entidade = dados;
        if(!this.entidade.fornecedor){
          this.entidade.fornecedor = new Fornecedor();
        }
        if(this.consulta){
          this.title.setTitle (`Gestop - Consulta de Produto:${this.entidade.descricao}`);
        }else{
          this.title.setTitle (`Gestop - Alteração de Produto :${this.entidade.descricao}`);
        }
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  salvar (form: FormControl) {
    if (this.entidade.codigo){
      this.alterar(form);
    } else {
      this.incluir(form);
    }
  }

  limparFormulario (form: FormControl){
    this.entidade = new Produto();
    form.reset();
  }

  exibiErroFormulario(form: FormControl){

    if(!form.value.descricao) {
      this.descricao = false;
    }else {
      this.descricao = true;
    }

    if(!form.value.codigoProprio) {
      this.codigoProprio = false;
    }else {
      this.codigoProprio = true;
    }

    if(!form.value.valorCusto) {
      this.valorCusto = false;
    }else {
      this.valorCusto = true;
    }

    if(!form.value.valorVenda) {
      this.valorVenda = false;
    }else {
      this.valorVenda = true;
    }

    if(!form.value.observacao) {
      this.observacao = false;
    }else {
      this.observacao = true;
    }

    if(!form.value.qtdAtual) {
      this.qtdAtual = false;
    }else {
      this.qtdAtual = true;
    }

    if(!form.value.qtdMinima) {
      this.qtdMinima = false;
    }else {
      this.qtdMinima = true;
    }

    if(!form.value.valorDesconto) {
      this.valorDesconto = false;
    }else {
      this.valorDesconto = true;
    }

    if(!form.value.valorMargem) {
      this.valorMargem = false;
    }else {
      this.valorMargem = true;
    }

    if(!form.value.embalagem) {
      this.embalagem = false;
    }else {
      this.embalagem = true;
    }

    if(!form.value.percentual) {
      this.percentual = false;
    }else {
      this.percentual = true;
    }

    if(!form.value.categoria) {
      this.categoria = false;
    }else {
      this.categoria = true;
    }

    if(!form.value.fornecedor) {
      this.fornecedor = false;
    }else {
      this.fornecedor = true;
    }
  }

  selecionarCategoria (categoria){
    this.entidade.categoria = categoria;
  }

  selecionarFornecedor (fornecedor){
    this.entidade.fornecedor = fornecedor;
  }
}
