import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

import { environment } from 'src/environments/environment';
import { SistemaHttp } from 'src/app/seguranca/sistema-http';
import { CrudService } from 'src/app/core/crud-service';
import { Produto } from './produto';

export class ProdutoFiltro {
  codigo: number;
  codigoProprio: number;
  descricao: string;
  pagina = 0;
  itensPorPagina: number;
}

@Injectable({
  providedIn: 'root'
})
export class ProdutoService extends CrudService<Produto>{

  private baseUrl = `${environment.apiUrl}/gestop-api/produto`;

  constructor(protected http: SistemaHttp) {
    super(http, `${environment.apiUrl}/gestop-api/produto`);
  }

  pesquisar(filtro: ProdutoFiltro): Promise<any> {

    let params = new HttpParams({
      fromObject: {
        page: filtro.pagina.toString(),
        size: filtro.itensPorPagina.toString()
      }
    });

    if(filtro.codigo) {
      params = params.append('codigo', filtro.codigo.toString());
    }

    if(filtro.descricao) {
      params = params.append('descricao', filtro.descricao);
    }

    return this.http.get<any>(this.baseUrl, { params })
      .toPromise()
      .then( response => {
        const resultado = {
          selecionados: response.content,
          total: response.totalElements
         }
        return resultado;
      });
  }

  gerarRelatorio(filtro: ProdutoFiltro){
    let params = new HttpParams();

    if(filtro.codigo) {
      params = params.append('codigo', filtro.codigo.toString());
    }

    if(filtro.descricao) {
      params = params.append('nome', filtro.descricao);
    }

  return this.http.get(`${this.baseUrl}/produto/relatorio`, { params, responseType: 'blob' })
    .toPromise();
}

}
