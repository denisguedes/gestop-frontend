export class Contato {
  codigo: number;
  email:string;
  telefone: string;

  constructor(codigo?: number, email?: string, telefone?: string){
      this.codigo = codigo;
      this.email = email;
      this.telefone = telefone;
  }

}
