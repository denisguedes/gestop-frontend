import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SegurancaGuard } from 'src/app/seguranca/seguranca.guard';
import { FormComponent } from './form/form.component';
import { ListComponent } from './list/list.component';

const routes: Routes = [
    {
      path: 'gestop/cargo/novo',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['CARGO_CADASTRAR']}
    },
    {
      path: 'gestop/cargo/listar',
      component: ListComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['CARGO_CONSULTAR']}
    },
    {
      path: 'gestop/cargo/:codigo',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['CARGO_ALTERAR']}
    },
    {
      path: 'gestop/cargo/:codigo/:consulta',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['CARGO_CONSULTAR']}
    },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})

export class CargoRoutingModule { }
