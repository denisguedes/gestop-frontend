import { Contato } from '../model/contato';
import { Endereco } from '../model/endereco';

export class Cliente {
  codigo: number;
  razaoSocial: string;
  nome: string;
  cpfCnpj: string;
  tipo: any;
  contatos = new Array<Contato>();
  endereco = new Endereco();

  constructor(codigo?: number, razaoSocial?: string, nome?: string){
    this.codigo = codigo;
    this.razaoSocial = razaoSocial;
    this.nome = nome;
}
}


