import { PedidoService } from './../../../financeiro/pedido/pedido.service';
import { ClienteService } from './../cliente.service';
import { Cliente } from './../cliente';
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { MenuItem, ConfirmationService } from 'primeng/components/common/api';
import { ToastrService } from 'ngx-toastr';
import { ErrorHandlerService } from 'src/app/core/error-handler.service';
import { Produto } from '../../produto/produto';
import { Pedido, PedidoItem } from 'src/app/financeiro/pedido/pedido';
import { DataLocaleService } from 'src/app/core/data-locale-service';
import { Contato } from '../../model/contato';
import { Endereco } from '../../model/endereco';
import { EstadoEnum } from 'src/app/core/estado-enum';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  private items: MenuItem[];
  private entidade = new Cliente();
  private consulta: boolean;
  private codigoEntidade: number;
  private rotaListar = "/gestop/cliente/listar";
  private cnpjCpfSelecionado: string = 'CNPJ';
  private contatosSelecionados: Contato[] = [];
  private contato = new Contato();
  private pedido = new Pedido();
  private produtosSelecionados: Produto[];
  private visibleDialogContato: boolean;
  private indexContato: number;
  private editandoContato: boolean;
  private valorTotalPedido = 0;
  private estados = new EstadoEnum();

  //Campos para validação
  private nome = true;
  private razaoSocial = true;
  private tipo = true;
  private status = true;
  private cpfCnpj = true;
  private endereco = true;
  private email = true;
  private telefone = true;
  private valor = true;
  private dataPedido = true;
  private dataVencimento = true;

  private tipoPagamento = [
    { label: 'Dinheiro', value: 'DINHEIRO' },
    { label: 'Cartão', value: 'CARTAO' },
  ]

  private parcelamento = [
    { label: '1 x 470,50', value: '1' },
    { label: '2 x 235,25', value: '2' },
    { label: '3 x 156,33', value: '3' },
  ]

  constructor(
    private entidadeService: ClienteService,
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private confirmation: ConfirmationService,
    private title: Title,
    private errorHandler : ErrorHandlerService,
    private locale: DataLocaleService,
    private pedidoService: PedidoService
  ) { }

  ngOnInit() {
    this.items = [
      {label:'Gestop'},
      {label:'Cadastro'},
      {label:'Cliente'}
    ];
    this.locale;
    this.valorTotalPedido;
    this.codigoEntidade = this.route.snapshot.params['codigo'];
    this.consulta = this.route.snapshot.params['consulta'];

    if(this.codigoEntidade){
      this.buscar(this.codigoEntidade);
    }

  }

  incluir (form: FormControl) {
    this.entidadeService.incluir(this.entidade)
      .then( ()=>{
        this.toastr.success('Cadastro realizado com sucesso');
        this.limparFormulario(form);
        this.router.navigate([this.rotaListar]);
      })
      .catch(erro => {
        this.errorHandler.handle(erro);
        this.exibiErroFormulario(form);
      });
  }

  alterar (form: FormControl) {
    this.entidadeService.alterar(this.entidade)
      .then( dados => {
        this.entidade = dados;
        this.toastr.success('Alteração realizada com sucesso');
        this.router.navigate([this.rotaListar]);
      })
      .catch(erro => {
        this.errorHandler.handle(erro)
        this.exibiErroFormulario(form);
      });
  }

  buscar (codigo: number) {
    this.entidadeService.buscar(codigo)
      .then( dados => {
        this.entidade = dados;
        if(!this.entidade.endereco){
          this.entidade.endereco = new Endereco();
        }
        this.preencherContatosSalvo();
        if(this.consulta){
          this.title.setTitle (`Gestop - Consulta de Cliente:${this.entidade.nome}`);
        }else{
          this.title.setTitle (`Gestop - Alteração de Cliente :${this.entidade.nome}`);
        }
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  salvar (form: FormControl) {

    this.preencherContatosParaSalvar();
    if (this.entidade.codigo){
      this.alterar(form);
    } else {
      this.incluir(form);
    }
  }

  limparFormulario (form: FormControl){
    this.entidade = new Cliente();
    form.reset();
  }

  exibiErroFormulario(form: FormControl){

    if(!form.value.nome) {
      this.nome = false;
    }else {
      this.nome = true;
    }

    if(!form.value.razaoSocial) {
      this.razaoSocial = false;
    }else {
      this.razaoSocial = true;
    }

    if(!form.value.tipo) {
      this.tipo = false;
    }else {
      this.tipo = true;
    }

    if(!form.value.cpfCnpj) {
      this.cpfCnpj = false;
    }else {
      this.cpfCnpj = true;
    }

    if(!form.value.endereco) {
      this.endereco = false;
    }else {
      this.endereco = true;
    }

    if(!form.value.valor) {
      this.valor = false;
    }else {
      this.valor = true;
    }

    if(!form.value.dataPedido) {
      this.dataPedido = false;
    }else {
      this.dataPedido = true;
    }

    if(!form.value.dataVencimento) {
      this.dataVencimento = false;
    }else {
      this.dataVencimento = true;
    }
  }

  salvarContato(contato: Contato) {

    if(contato.email || contato.telefone){

      if(this.editandoContato){
        this.contatosSelecionados[this.indexContato] = new Contato(contato.codigo, contato.email, contato.telefone);
      }else{
        this.contatosSelecionados.push(contato);
      }
      this.contato = new Contato();
      this.visibleDialogContato = false;
      this.editandoContato = false;

    }else{
        this.toastr.error('Informe o telefone ou e-mail!');
        this.email = false;
        this.telefone = false;
    }
  }

  editarContato(contato: Contato, index: number) {
    this.contato = new Contato(contato.codigo, contato.email, contato.telefone);
    this.indexContato = index;
    this.editandoContato = true
    this.visibleDialogContato = true;
  }

  removerContato(index: number) {
    this.confirmation.confirm({
      message: 'Tem certeza que deseja excluir',
      accept: (() => {
        this.contatosSelecionados.splice(index, 1);
        this.toastr.success('Item excluído com sucesso!');
      })
    });
  }

  showDialogContato(){
    this.visibleDialogContato = true;
  }

  cancelarModal(){
    this.visibleDialogContato = false;
  }

  preencherContatosParaSalvar(){

    this.entidade.contatos = new Array<Contato>();

    for(let i = 0; i < this.contatosSelecionados.length; i++) {
      let contato: Contato = this.contatosSelecionados[i];
      this.entidade.contatos.push(new Contato(contato.codigo, contato.email, contato.telefone));
    }
  }

  preencherContatosSalvo() {
    for(let i = 0; i < this.entidade.contatos.length; i++) {
      let contato = this.entidade.contatos[i];
      this.contatosSelecionados.push(contato);
    }
  }

  selecionarProduto(produtos: Produto[]){
    this.produtosSelecionados = produtos;

    this.produtosSelecionados.forEach(element => {
      this.valorTotalPedido += element.valorVenda;
    });
  }

  finalizarPedido(pedido: Pedido){
    pedido.valorPedido = this.valorTotalPedido;
    pedido.cliente = this.entidade;

    this.produtosSelecionados.forEach(entidade => {
      let pedidoItem = new PedidoItem(entidade);
      pedido.pedidosItem.push(pedidoItem);
    });

    this.pedidoService.incluir(pedido)
    .then( ()=>{
      this.toastr.success('Pedido realizado com sucesso');
      this.router.navigate([this.rotaListar]);
    })
    .catch(erro => {
      this.errorHandler.handle(erro);
    });
  }

}
