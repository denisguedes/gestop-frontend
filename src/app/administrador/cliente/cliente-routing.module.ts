
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SegurancaGuard } from 'src/app/seguranca/seguranca.guard';
import { FormComponent } from './form/form.component';
import { ListComponent } from './list/list.component';

const routes: Routes = [
    {
      path: 'gestop/cliente/novo',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['CLIENTE_CADASTRAR']}
    },
    {
      path: 'gestop/cliente/listar',
      component: ListComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['CLIENTE_CONSULTAR']}
    },
    {
      path: 'gestop/cliente/:codigo',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['CLIENTE_ALTERAR']}
    },
    {
      path: 'gestop/cliente/:codigo/:consulta',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['CLIENTE_CONSULTAR']}
    },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class ClienteRoutingModule { }
