import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SegurancaGuard } from 'src/app/seguranca/seguranca.guard';
import { FormComponent } from './form/form.component';
import { ListComponent } from './list/list.component';

const routes: Routes = [
    {
      path: 'gestop/forma-pagamento/novo',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['FORMA_PAGAMENTO_CADASTRAR']}
    },
    {
      path: 'gestop/forma-pagamento/listar',
      component: ListComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['FORMA_PAGAMENTO_CONSULTAR']}
    },
    {
      path: 'gestop/forma-pagamento/:codigo',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['FORMA_PAGAMENTO_ALTERAR']}
    },
    {
      path: 'gestop/forma-pagamento/:codigo/:consulta',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['FORMA_PAGAMENTO_CONSULTAR']}
    },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})

export class FormaPagamentoRoutingModule { }
