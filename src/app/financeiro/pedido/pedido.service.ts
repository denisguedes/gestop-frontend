import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

import { environment } from 'src/environments/environment';
import { SistemaHttp } from 'src/app/seguranca/sistema-http';
import { CrudService } from 'src/app/core/crud-service';
import { Pedido } from './pedido';
import { Cliente } from 'src/app/administrador/cliente/cliente';
import { Data } from '@angular/router';
import * as moment from 'moment';
import { Pagamento } from '../pagamento/pagamento';

export class PedidoFiltro {
  codigo: number;
  dataPedido: any;
  cliente = new Cliente();
  pagina = 0;
  itensPorPagina: number;
}

@Injectable({
  providedIn: 'root'
})
export class PedidoService extends CrudService<Pedido>{

  private baseUrl = `${environment.apiUrl}/gestop-api/pedido`;

  constructor(protected http: SistemaHttp) {
    super(http, `${environment.apiUrl}/gestop-api/pedido`);
  }

  pesquisar(filtro: PedidoFiltro): Promise<any> {

    let params = new HttpParams({
      fromObject: {
        page: filtro.pagina.toString(),
        size: filtro.itensPorPagina.toString()
      }
    });

    if(filtro.codigo) {
      params = params.append('codigo', filtro.codigo.toString());
    }

    if(filtro.dataPedido) {
      params = params.append('dataPedido', filtro.dataPedido);
    }

    if(filtro.cliente.nome) {
      params = params.append('nome', filtro.cliente.nome);
    }

    return this.http.get<any>(this.baseUrl, { params })
      .toPromise()
      .then( response => {
        const resultado = {
          selecionados: response.content,
          total: response.totalElements
         }
        return resultado;
      });
  }

  verificarAgendamento(dataInicio: Data, dataFim: Data, periodo: string): Promise<any>{

    let params = new HttpParams();
    params = params.append('dataInicio', moment(dataInicio).format('YYYY-MM-DD'));
    params = params.append('dataFim', moment(dataFim).format('YYYY-MM-DD'));
    params = params.append('periodo', periodo);
    return this.http.get<any>(`${this.baseUrl}/agendamento`, {params})
    .toPromise()
    .then( response => {
      return response
    });
  }

}
