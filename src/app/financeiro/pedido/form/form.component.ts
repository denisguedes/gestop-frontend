import { FuncionarioService } from './../../../administrador/funcionario/funcionario.service';
import { Funcionario } from './../../../administrador/funcionario/funcionario';

import { FormaPagamentoService } from './../../forma-pagamento/forma-pagamento.service';
import { FormaPagamento } from './../../forma-pagamento/forma-pagamento';
import { PedidoService } from './../../../financeiro/pedido/pedido.service';
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router, ActivatedRoute, Data } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { MenuItem, ConfirmationService } from 'primeng/components/common/api';
import { ToastrService } from 'ngx-toastr';
import { ErrorHandlerService } from 'src/app/core/error-handler.service';
import { Pedido, PedidoItem } from 'src/app/financeiro/pedido/pedido';
import { DataLocaleService } from 'src/app/core/data-locale-service';
import { Produto } from 'src/app/administrador/produto/produto';
import { Cliente } from 'src/app/administrador/cliente/cliente';
import { Pagamento } from '../../pagamento/pagamento';
import { PagamentoService, PagamentoFiltro } from '../../pagamento/pagamento.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  private items: MenuItem[];
  private entidade = new Pedido();
  private consulta: boolean;
  private codigoEntidade: number;
  private rotaListar = "/gestop/pedido/listar";
  private pedido = new Pedido();
  private produtosSelecionados: Produto[] = [];
  private visibleDialogContato: boolean;
  private clienteSelecionado = new Cliente();
  private indexContato: number;
  private editandoContato: boolean;
  private pagamentos: Pagamento[] = [];
  private parcelas: any[] = [];
  private formasPagamento: FormaPagamento[] = [];
  private funcionarios: Funcionario[] = [];
  private pagamentoFinalizado = false;
  private pagamentoSelecionado = new Pagamento();

  private periodos = [
    { label: 'Manhã', value: 'MANHA' },
    { label: 'Tarde', value: 'TARDE' },
    { label: 'Noite', value: 'NOITE' },
  ];

  //Campos para validação
  private nome = true;
  private razaoSocial = true;
  private cidade = true;
  private tipo = true;
  private status = true;
  private cpfCnpj = true;
  private endereco = true;
  private email = true;
  private telefone = true;
  private valor = true;
  private dataPedido = true;
  private dataVencimento = true;
  private valorDesconto = true;
  private valorEntrada = true;
  private cliente = true;
  private pagamento = true;
  private dataInicio = true;
  private dataFim = true;
  private periodo = true;
  private funcionario = true;

  constructor(
    private entidadeService: PedidoService,
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private confirmation: ConfirmationService,
    private title: Title,
    private errorHandler : ErrorHandlerService,
    private locale: DataLocaleService,
    private formaPagamentoService: FormaPagamentoService,
    private pagamentoService: PagamentoService,
    private funcionarioService: FuncionarioService
  ) { }

  ngOnInit() {
    this.items = [
      {label:'Gestop'},
      {label:'Cadastro'},
      {label:'Pedido'}
    ];
    this.buscarFormasPagamento();
    this.locale;
    this.codigoEntidade = this.route.snapshot.params['codigo'];
    this.consulta = this.route.snapshot.params['consulta'];
    this.buscarFuncionarios();
    if(this.codigoEntidade){
      this.pagamentoFinalizado = true;
      this.buscar(this.codigoEntidade);
    }
  }

  buscarFuncionarios(){
    this.funcionarioService.listar().then( element => {
      this.funcionarios = element.map(c => ({ label: c.nome, value: c.codigo }));
    })
    .catch(erro => {
      this.errorHandler.handle(erro);
    });
  }

  finalizarPagamento(){
    if(!this.pagamentoSelecionado.formaPagamento || !this.pagamentoSelecionado.dataVencimento){
      this.toastr.error("Preencher forma de pagamento e/ou data de vencimento");
    }else{
      this.entidade.pagamentos = [];
      let pagamentoParcela = new PagamentoFiltro();
      let codigo: any = this.pagamentoSelecionado.formaPagamento.descricao;
      this.pagamentoService.parcelamento(codigo, this.calculoVenda(), this.pagamentoSelecionado.numero, this.pagamentoSelecionado.dataVencimento)
      .then(element => {
        this.entidade.pagamentos = element;
      })
      .catch(erro => this.errorHandler.handle(erro));
        this.pagamentoFinalizado = true;
      }
  }

  calculoVenda(){
    return (this.entidade.valorPedido - this.entidade.valorDesconto) - this.entidade.valorEntrada;
  }

  parcelamento(){
    let selecionarPagamento:Pagamento[] = [];
    for(let i = 1; i <= 10; i++) {
      let pagamento = new Pagamento();
      pagamento.valorParcela =  this.calculoVenda() / i;
      pagamento.numero = i;
      selecionarPagamento.push(pagamento);
      this.parcelas = selecionarPagamento.map(c => ({ label: c.numero + ' X ' + c.valorParcela.toFixed(2).replace('.', ','), value: c.numero }));
    }
  }

  buscarFormasPagamento() {
    this.formaPagamentoService.listar().then(lista => {
      this.formasPagamento = lista.map(c => ({ label: c.descricao, value: c.codigo }));
    })
    .catch(erro => this.errorHandler.handle(erro));
  }

  alterar (form: FormControl) {
    this.entidadeService.alterar(this.entidade)
      .then( dados => {
        this.entidade = dados;
        this.toastr.success('Alteração realizada com sucesso');
        this.router.navigate([this.rotaListar]);
      })
      .catch(erro => {
        this.errorHandler.handle(erro)
        this.exibiErroFormulario(form);
      });
  }

  buscar (codigo: number) {
    this.entidadeService.buscar(codigo)
      .then( dados => {
        console.log(dados);

        this.entidade = dados;

        this.entidade.dataPedido = new Date(this.entidade.dataPedido);
        this.entidade.dataInicio = new Date(this.entidade.dataInicio);
        this.entidade.dataFim = new Date(this.entidade.dataFim);

        this.parcelamento();

        this.entidade.pedidosItem.forEach( element => {
          this.produtosSelecionados.push(element.produto);
        });

        if(this.consulta){
          this.title.setTitle (`Gestop - Consulta de Pedido:${this.entidade.cliente.nome}`);
        }else{
          this.title.setTitle (`Gestop - Alteração de Pedido :${this.entidade.cliente.nome}`);
        }
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  selecionarCliente (cliente){
    this.entidade.cliente = cliente;
  }

  finalizarPedido(){
    this.entidadeService.incluir(this.entidade)
    .then( ()=>{
      this.toastr.success('Pedido finalizado com sucesso');
      this.router.navigate([this.rotaListar]);
    })
    .catch(erro => {
      this.errorHandler.handle(erro);
    });
  }

  limparFormulario (form: FormControl){
    this.entidade = new Pedido();
    form.reset();
  }

  selecionarProduto(produtos: Produto[]){
    this.produtosSelecionados = produtos;

    produtos.forEach( produto => {
      let pedidoItem = new PedidoItem();
      pedidoItem.qtdProduto = produto.quantidade;
      pedidoItem.valorProduto = produto.valorVenda * pedidoItem.qtdProduto;
      pedidoItem.produto = produto;
      this.entidade.pedidosItem.push(pedidoItem);
    });
    if(this.entidade.valorPedido > 0) {

    }else{
    this.entidade.valorPedido = 0;
    }
      produtos.forEach(element => {
      this.entidade.valorPedido += (element.valorVenda * element.quantidade);
    });

      this.parcelamento();
      this.pagamentoFinalizado = false;
  }

  removerProduto(index: number) {
    this.confirmation.confirm({
      message: 'Tem certeza que deseja excluir',
      accept: (() => {
        this.entidade.pedidosItem.splice(index, 1);
        this.produtosSelecionados.splice(index, 1)
        this.toastr.success('Item excluído com sucesso!');
        this.entidade.valorPedido = 0;
        this.entidade.pedidosItem.forEach( element => {
        this.entidade.valorPedido += (element.produto.valorVenda * element.qtdProduto);
        });
      })
    });
  }

  cancelar (){
    let msg = "";
    if(this.entidade['codigo']){
      msg = "Tem certeza que deseja descartar a alteração";
    }else{
      msg = "Tem certeza que deseja descartar a inclusão";
    }

    this.confirmation.confirm({
      message: msg,
      accept: (() => {
        this.router.navigate([this.rotaListar]);
      })
    });
  }

  verificarAgendamento(dataInicio: Data, dataFim: Data, periodo: string) {
      this.entidadeService.verificarAgendamento(dataInicio, dataFim, periodo)
      .then( resultado => {
        resultado.forEach( pedido => {
          if(pedido.codigo !== null) {
            pedido.pedidosItem.forEach( itemPedido => {

              this.produtosSelecionados.forEach( produto => {
                if(itemPedido.produto.descricao === produto.descricao) {
                  this.entidade.periodo = '';
                   this.toastr.error('Produto ' + produto.descricao + ' já possui agendamento para data informada!');
                }
              });
            });
          }
        });

      }).catch( erro => { this.errorHandler.handle(erro)});
  }

  exibiErroFormulario(form: FormControl){

    if(!form.value.nome) {
      this.nome = false;
    }else {
      this.nome = true;
    }

    if(!form.value.razaoSocial) {
      this.razaoSocial = false;
    }else {
      this.razaoSocial = true;
    }

    if(!form.value.tipo) {
      this.tipo = false;
    }else {
      this.tipo = true;
    }

    if(!form.value.cpfCnpj) {
      this.cpfCnpj = false;
    }else {
      this.cpfCnpj = true;
    }

    if(!form.value.endereco) {
      this.endereco = false;
    }else {
      this.endereco = true;
    }

    if(!form.value.valor) {
      this.valor = false;
    }else {
      this.valor = true;
    }

    if(!form.value.valorDesconto) {
      this.valorDesconto = false;
    }else {
      this.valorDesconto = true;
    }

    if(!form.value.valorEntrada) {
      this.valorEntrada = false;
    }else {
      this.valorEntrada = true;
    }

    if(!form.value.dataPedido) {
      this.dataPedido = false;
    }else {
      this.dataPedido = true;
    }

    if(!form.value.dataVencimento) {
      this.dataVencimento = false;
    }else {
      this.dataVencimento = true;
    }

    if(!form.value.cliente) {
      this.cliente = false;
    }else {
      this.cliente = true;
    }

    if(!form.value.pagamento) {
      this.pagamento = false;
    }else {
      this.pagamento = true;
    }

    if(!form.value.tipo) {
      this.tipo = false;
    }else {
      this.tipo = true;
    }

    if(!form.value.dataInicio) {
      this.dataInicio = false;
    }else {
      this.dataInicio = true;
    }

    if(!form.value.dataFim) {
      this.dataFim = false;
    }else {
      this.dataFim = true;
    }

    if(!form.value.periodo) {
      this.periodo = false;
    }else {
      this.periodo = true;
    }

    if(!form.value.funcionario) {
      this.funcionario = false;
    }else {
      this.funcionario = true;
    }
  }

}
