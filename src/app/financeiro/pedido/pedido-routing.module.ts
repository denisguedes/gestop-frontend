import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FormComponent } from './form/form.component';
import { ListComponent } from './list/list.component';
import { SegurancaGuard } from 'src/app/seguranca/seguranca.guard';

const routes: Routes = [
    {
      path: 'gestop/pedido/novo',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['PEDIDO_CADASTRAR']}
    },
    {
      path: 'gestop/pedido/listar',
      component: ListComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['PEDIDO_CONSULTAR']}
    },
    {
      path: 'gestop/pedido/:codigo',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['PEDIDO_ALTERAR']}
    },
    {
      path: 'gestop/pedido/:codigo/:consulta',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['PEDIDO_CONSULTAR']}
    },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})

export class PedidoRoutingModule { }
