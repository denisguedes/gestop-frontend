import { Funcionario } from './../../administrador/funcionario/funcionario';
import { Cliente } from 'src/app/administrador/cliente/cliente';
import { Produto } from 'src/app/administrador/produto/produto';
import { Pagamento } from '../pagamento/pagamento';

export class Pedido {
    codigo: number;
    valorPedido: number;
    dataPedido: Date;
    dataInicio: Date;
    dataFim: Date;
    periodo: string;
    valorDesconto: number = 0;
    valorEntrada: number = 0;
    tipo: string;
    cliente = new Cliente();
    funcionario = new Funcionario();
    pagamentos = new Array<Pagamento>();
    pedidosItem = new Array<PedidoItem>();

    constructor(){
      this.cliente = new Cliente();
    }
}

export class PedidoItem {
    codigo: number;
    qtdProduto: number;
    valorProduto: number;
    produto = new Produto();

    constructor(produto?: Produto){
      this.produto = produto;
  }


}
