import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import {InputTextModule} from 'primeng/inputtext';
import {ButtonModule} from 'primeng/button';
import {TableModule} from 'primeng/table';
import {TooltipModule} from 'primeng/tooltip';
import {FieldsetModule} from 'primeng/fieldset';
import {BreadcrumbModule} from 'primeng/breadcrumb';
import {RadioButtonModule} from 'primeng/radiobutton';
import {DropdownModule} from 'primeng/dropdown';
import {PickListModule} from 'primeng/picklist';
import {AccordionModule} from 'primeng/accordion';
import {CalendarModule} from 'primeng/calendar';
import {CheckboxModule} from 'primeng/checkbox';
import {InputMaskModule} from 'primeng/inputmask';
import {CurrencyMaskModule} from "ng2-currency-mask";

import { ComponentesModule } from 'src/app/componentes/componentes.module';
import { FormComponent } from './form/form.component';
import { ListComponent } from './list/list.component';
import { PedidoRoutingModule } from './pedido-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,

    ComponentesModule,
    PedidoRoutingModule,

    InputTextModule,
    ButtonModule,
    TableModule,
    TooltipModule,
    FieldsetModule,
    BreadcrumbModule,
    RadioButtonModule,
    DropdownModule,
    PickListModule,
    AccordionModule,
    CalendarModule,
    CheckboxModule,
    InputMaskModule,
    CurrencyMaskModule
  ],
  declarations: [
    FormComponent,
    ListComponent
  ]
})
export class PedidoModule { }
