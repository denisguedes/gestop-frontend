import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SegurancaGuard } from 'src/app/seguranca/seguranca.guard';
import { ListComponent } from './list/list.component';

const routes: Routes = [

    {
      path: 'gestop/pagamento/listar',
      component: ListComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['PAGAMENTO_CONSULTAR']}
    },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class PagamentoRoutingModule { }
