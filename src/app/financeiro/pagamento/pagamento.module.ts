import { InputMaskModule } from 'primeng/inputmask';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import {InputTextModule} from 'primeng/inputtext';
import {ButtonModule} from 'primeng/button';
import {TableModule} from 'primeng/table';
import {TooltipModule} from 'primeng/tooltip';
import {FieldsetModule} from 'primeng/fieldset';
import {BreadcrumbModule} from 'primeng/breadcrumb';
import {RadioButtonModule} from 'primeng/radiobutton';
import {DropdownModule} from 'primeng/dropdown';
import {CurrencyMaskModule} from "ng2-currency-mask";

import { ComponentesModule } from 'src/app/componentes/componentes.module';
import { ListComponent } from './list/list.component';
import { RelatorioComponent } from './relatorio/relatorio.component';
import { PagamentoRoutingModule } from './pagamento-routing.module';
import { PickListModule } from 'primeng/picklist';
import { AccordionModule } from 'primeng/accordion';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,

    ComponentesModule,
    PagamentoRoutingModule,

    InputTextModule,
    ButtonModule,
    TableModule,
    TooltipModule,
    FieldsetModule,
    BreadcrumbModule,
    RadioButtonModule,
    DropdownModule,
    PickListModule,
    AccordionModule,
    InputMaskModule,
    CurrencyMaskModule
  ],
  declarations: [
    ListComponent,
    RelatorioComponent
  ]
})
export class PagamentoModule { }
