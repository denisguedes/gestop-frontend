import { FormaPagamento } from '../forma-pagamento/forma-pagamento';
import { Pedido } from '../pedido/pedido';

export class Pagamento {
    codigo: number;
    valorParcela: number;
    valorPagamento: number;
    numero: number;
    status: string;
    dataVencimento: Date;
    dataPagamento: Date;
    pedido = new Pedido();
    formaPagamento = new FormaPagamento();

    constructor(codigo?: number, dataVencimento?: Date){
      this.codigo = codigo;
      this.dataVencimento = dataVencimento;
      this.formaPagamento = new FormaPagamento();
      this.pedido = new Pedido();
    }
}

