import { Pagamento } from './../pagamento';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';

import { MenuItem, LazyLoadEvent, ConfirmationService } from 'primeng/components/common/api';
import { ToastrService } from 'ngx-toastr';

import { SegurancaService } from 'src/app/seguranca/seguranca.service';
import { ErrorHandlerService } from 'src/app/core/error-handler.service';
import { Pedido } from '../../pedido/pedido';
import { PedidoFiltro, PedidoService } from '../../pedido/pedido.service';
import { PagamentoService } from '../pagamento.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  private items: MenuItem[];
  private quantidadeItemPorPagina: any[];
  private quantidadeItemPorPaginaSelecionada: any;
  private selecionados: Pedido[];
  private filtro = new PedidoFiltro;
  private totalRegistros = 0;
  private codigoEntidade: number;
  private consultar = true;
  private rotaNovo = "/gestop/pedido/novo";
  private rotaPadrao = '/gestop/pedido/';
  @ViewChild('tabela') tabelaEntidade;

  constructor(
    private entidadeService: PedidoService,
    private pagamentoService: PagamentoService,
    private title: Title,
    private toastr: ToastrService,
    private errorHandler: ErrorHandlerService,
    private confirmation: ConfirmationService,
    private router: Router,
    private segurancaService: SegurancaService
  ) {}

  ngOnInit() {

    this.pesquisar();

    this.items = [
      {label:'Gestop'},
      {label:'Cadastro'},
      {label:'Listagem de Pagamentos'}
    ];

    this.quantidadeItemPorPagina = [
      {value:10},
      {value:20},
      {value:50},
    ]

    this.title.setTitle ('Gestop - Pagamento');
  }

  pagamentoParcela(pagamento: Pagamento, pedido: Pedido){
    let pedidoSelecionado = new Pedido();
    pedidoSelecionado.codigo = pedido.codigo;
    pagamento.pedido = pedidoSelecionado;
    pagamento.dataPagamento = new Date();
    pagamento.status = 'PAGO';

    this.pagamentoService.alterar(pagamento)
      .then( dados => {
        pagamento = dados;
        this.toastr.success('Pagamento realizado!');
      })
      .catch(erro => {
        this.errorHandler.handle(erro)
      });
  }

  pesquisar(pagina = 0) {
    this.filtro.pagina = pagina;
    this.filtro.itensPorPagina = this.quantidadeItemPorPaginaSelecionada ? this.quantidadeItemPorPaginaSelecionada.value : 10;

    this.entidadeService.pesquisar(this.filtro)
      .then(resultado => {
        this.totalRegistros = resultado.total;
        this.selecionados = resultado.selecionados;
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  relatorio(codigo: number) {
    console.log(codigo);

    this.pagamentoService.gerarRelatorio();
  }

  limparFiltro () {
    this.filtro = new PedidoFiltro();
    this.pesquisar();
  }

  aoMudarPagina (event: LazyLoadEvent) {
    const pagina = event.first / event.rows;
    this.pesquisar(pagina);
  }

  selecionarEntidade (entidade){
    this.codigoEntidade = entidade.codigo;
  }

}
