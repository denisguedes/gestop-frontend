import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

import { environment } from 'src/environments/environment';
import { SistemaHttp } from 'src/app/seguranca/sistema-http';
import { CrudService } from 'src/app/core/crud-service';
import { Pagamento } from './pagamento';
import * as moment from 'moment';

export class PagamentoFiltro {
  codigo: number;
    valorParcela: number;
    valorPagamento: number;
    numero: number;
    status: string;
    dataVencimento: Date;
    dataPagamento: Date;
  pagina = 0;
  itensPorPagina: number;
}

@Injectable({
  providedIn: 'root'
})
export class PagamentoService extends CrudService<Pagamento>{

  private baseUrl = `${environment.apiUrl}/gestop-api/pagamento`;

  constructor(protected http: SistemaHttp) {
    super(http, `${environment.apiUrl}/gestop-api/pagamento`);
  }

  pesquisar(filtro: PagamentoFiltro): Promise<any> {

    let params = new HttpParams({
      fromObject: {
        page: filtro.pagina.toString(),
        size: filtro.itensPorPagina.toString()
      }
    });

    if(filtro.codigo) {
      params = params.append('codigo', filtro.codigo.toString());
    }

    return this.http.get<any>(this.baseUrl, { params })
      .toPromise()
      .then( response => {
        const resultado = {
          selecionados: response.content,
          total: response.totalElements
         }
        return resultado;
      });
  }

  parcelamento(codigo: number, valor: number, numero: number, dataVencimento: Date): Promise<any> {

    let params = new HttpParams();
    params = params.append('codigo', codigo.toString());
    params = params.append('valor', valor.toString());
    params = params.append('numero', numero.toString());
    params = params.append('dataVencimento', moment(dataVencimento).format('YYYY-MM-DD'));

    // return this.http.get(`${this.baseUrl}/parcelamento/${codigo}/${valor}/${numero}/${moment(dataVencimento).format('YYYY-MM-DD')}`)
    return this.http.get(`${this.baseUrl}/parcelamento`, { params })

    .toPromise()
    .then( response => {
      return response;
    });

  }

  gerarRelatorio(){
    console.log('Gerando relatorio2');
    let params = new HttpParams();

  return this.http.get(`${this.baseUrl}/pagamento/relatorio`, { params, responseType: 'blob' })
    .toPromise();
}

}
