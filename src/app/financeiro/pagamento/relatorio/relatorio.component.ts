import { Component, OnInit } from '@angular/core';
import { PagamentoFiltro, PagamentoService } from '../pagamento.service';

@Component({
  selector: 'app-relatorio',
  templateUrl: './relatorio.component.html',
  styleUrls: ['./relatorio.component.css']
})
export class RelatorioComponent implements OnInit {

  private filtro = new PagamentoFiltro

  constructor(
    private pagamentoService: PagamentoService) { }

  ngOnInit() {
  }

  gerar() {
    this.pagamentoService.gerarRelatorio()
      .then(relatorio => {
        const url = window.URL.createObjectURL(relatorio);

        window.open(url);
      });
  }
}
