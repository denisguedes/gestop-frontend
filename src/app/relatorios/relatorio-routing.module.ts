import { SegurancaGuard } from 'src/app/seguranca/seguranca.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RelatorioLancamentosComponent } from './relatorio-pagamentos/relatorio-lancamentos.component';

const routes: Routes = [
  {
    path: 'gestop/relatorio/contasPagar',
    component: RelatorioLancamentosComponent,
    canActivate: [ SegurancaGuard ],
    data: { roles: ['PAGAMENTO_CONSULTAR'] }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RelatorioRoutingModule { }
