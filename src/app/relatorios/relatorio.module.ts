import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { CalendarModule } from 'primeng/calendar';

import { SharedModule } from './../shared/shared.module';
import { RelatorioRoutingModule } from './relatorio-routing.module';
import { RelatorioLancamentosComponent } from './relatorio-pagamentos/relatorio-lancamentos.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,

    CalendarModule,

    SharedModule,
    RelatorioRoutingModule
  ],
  declarations: [RelatorioLancamentosComponent]
})
export class RelatorioModule { }
