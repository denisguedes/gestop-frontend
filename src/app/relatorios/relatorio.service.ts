import { CrudService } from './../core/crud-service';
import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

import * as moment from 'moment';

import { environment } from './../../environments/environment';
import { SistemaHttp } from '../seguranca/sistema-http';
import { Pedido } from '../financeiro/pedido/pedido';

@Injectable()
export class RelatorioService extends CrudService<Pedido>{

  private baseUrl = `${environment.apiUrl}/gestop-api/relatorio`;

  constructor(protected http: SistemaHttp) {
    super(http, `${environment.apiUrl}/gestop-api/relatorio`);
  }

  relatorioContasPagar(inicio: Date, fim: Date) {
    const params = new HttpParams()
      .append('inicio', moment(inicio).format('YYYY-MM-DD'))
      .append('fim', moment(fim).format('YYYY-MM-DD'));

    return this.http.get(`${this.baseUrl}/contasPagar`,
      { params, responseType: 'blob' })
      .toPromise();
  }
}
